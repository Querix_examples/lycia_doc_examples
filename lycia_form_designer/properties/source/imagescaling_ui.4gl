##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE bt_ui ui.Button
DEFINE image_ui ui.Image
DEFINE bt1 STRING
DEFINE bt_val CHAR(16)

OPEN WINDOW n WITH FORM "imagescaling_ui" ATTRIBUTE(BORDER) 

LET bt_ui = ui.Button.forName("bt1")

CALL image_ui.setImageUrl("qx://application/large_lycia.png")
CALL image_ui.setImageScaling("None")
CALL bt_ui.SetImage(image_ui)
LET bt_val = image_ui.getImageScaling()
    
MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display" ,"display" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "act_bt"
    CASE bt_val
      WHEN "None"
        CALL image_ui.setImageScaling("Vertical")
        CALL bt_ui.SetImage(image_ui)
        LET bt_val = image_ui.getImageScaling()

      WHEN "Vertical"
        CALL image_ui.setImageScaling("Horizontal")
        CALL bt_ui.SetImage(image_ui)
        LET bt_val = image_ui.getImageScaling()
  
      WHEN "Horizontal"
        CALL image_ui.setImageScaling("Both")
        CALL bt_ui.SetImage(image_ui)
        LET bt_val = image_ui.getImageScaling()
  
      WHEN "Both"
        CALL image_ui.setImageScaling("None")
        CALL bt_ui.SetImage(image_ui)
        LET bt_val = image_ui.getImageScaling()
    END CASE
  
  ON ACTION "display"
    DISPLAY bt_val
  
  ON ACTION "exit"
    EXIT MENU
  END MENU

END MAIN