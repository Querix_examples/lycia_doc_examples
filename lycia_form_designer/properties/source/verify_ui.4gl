##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING
DEFINE tf1, tf2 ui.TextField

OPEN WINDOW w WITH FORM "verify_ui" ATTRIBUTE(BORDER)
LET tf1 = ui.TextField.ForName("f1")
LET tf2 = ui.TextField.ForName("f2")

CALL tf1.getVerify()
CALL tf1.setVerify(1)
CALL tf1.getVerify()
CALL tf2.setVerify(0)

INPUT BY NAME f1
INPUT BY NAME f2

END MAIN