##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE ta1, ta2 ui.TextArea
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "maxlength_ui" ATTRIBUTE(BORDER)
LET ta1 = ui.TextArea.ForName("f1")
LET ta2 = ui.TextArea.ForName("f2")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/build.svg")
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
  COMMAND "set"
    CALL ta1.SetMaxLength(10)
    CALL ta2.SetMaxLength(5)
  COMMAND "input"
    INPUT BY NAME f1 WITHOUT DEFAULTS
    INPUT BY NAME f2 WITHOUT DEFAULTS
  COMMAND "get"
    DISPLAY "With f1, maxLength = ", ta1.getMaxLength()
    DISPLAY "With f2, maxLength = ", ta2.getMaxLength()
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN