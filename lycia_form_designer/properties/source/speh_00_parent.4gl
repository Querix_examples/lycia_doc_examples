##########################################################################
# Created by Eugenia Chubar   			                                 #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
CALL ui.Interface.setType("container")
CALL ui.Application.GetCurrent().setMenuType("Tree")
CALL ui.Interface.LoadStartMenu("speh_00_parent")

CALL fgl_getkey()
END MAIN