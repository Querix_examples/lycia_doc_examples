##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING
DEFINE cld1, cld2 ui.Calendar

OPEN WINDOW w WITH FORM "required_ui" ATTRIBUTE(BORDER)
LET cld1 = ui.Calendar.ForName("f1")
LET cld2 = ui.Calendar.ForName("f2")

CALL cld1.setRequired(1)
CALL cld2.setRequired(0)

INPUT BY NAME f1
INPUT BY NAME f2

END MAIN