##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE grd ui.GridPanel
DEFINE root_size ui.Size

OPEN WINDOW w WITH FORM "preferredsize_ui" ATTRIBUTE(BORDER)
LET grd = ui.GridPanel.ForName("rootContainer")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("change as record" ,"change as record" , "qx://application/flip_to_back.svg")
    CALL fgl_setactionlabel("change as array" ,"change as array" , "qx://application/flip_to_front.svg")
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
  ON ACTION "change as record"
    LET root_size.width="500px"
    LET root_size.height="400px"
    CALL grd.setPreferredSize(root_size)
  ON ACTION "change as array"
    CALL grd.setPreferredSize(["250px", "150px"])
  ON ACTION "get"
    DISPLAY grd.getPreferredSize()
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN