##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1 DATE
DEFINE input_var, tmp STRING
DEFINE cld ui.Calendar

OPEN WINDOW w WITH FORM "format_date_ui" ATTRIBUTE(BORDER)

LET cld = ui.Calendar.ForName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("display", "display", "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "input"
    IF UPSHIFT(fgl_winquestion("", "Do you know how to define the display format for dates?", "Yes", "Yes|No", "question", 1))="YES" THEN
      LET input_var = fgl_winprompt(5, 3, "Define the format to display your input." , "", 25, 0)
      IF input_var IS NOT NULL THEN
        CALL cld.setEnable(1)
        CALL cld.setFormat(input_var)
        INPUT BY NAME f1
      END IF
    ELSE
      OPEN WINDOW w1 AT 5,5 WITH FORM "info_format_date" ATTRIBUTE(BORDER)
      CALL fgl_getkey()
      CLOSE WINDOW w1
      LET input_var = fgl_winprompt(5, 3, "Define the format to display your input." , "", 25, 0)
      IF input_var IS NOT NULL THEN
        CALL cld.setEnable(1)
        CALL cld.setFormat(input_var)
        INPUT BY NAME f1
      END IF
    END IF
  ON ACTION "display"
    LET tmp = "format: ", cld.getFormat(), ""
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN