##########################################################################
# Created by Alexey Printsevsky                                          #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE isBorder BOOLEAN
MAIN
DEFINE msg STRING
	OPEN WINDOW wmain WITH FORM "main"
	LET isBorder = FALSE
	CALL fgl_setactionlabel("cancel","")
	CALL fgl_setactionlabel("accept","Return")
	INPUT BY NAME isBorder WITHOUT DEFAULTS
		ON CHANGE isBorder
			CALL fgl_dialog_update_data()
 		ON ACTION "form1"
			IF isBorder THEN
 				LET msg = "RootContainer aligns by dialog window with size defined in form 300x200px and can be resized"
 			ELSE
 				LET msg = "RootContainer aligns by the client's window. Can be resized with client's window resizing"
 			END IF
 			CALL OpenWindowWithForm("str_str_pref",msg)
	 	ON ACTION "form2"
			IF isBorder THEN
 				LET msg = "RootContainer aligns by dialog window with default size 600x400px and can be resized"
 			ELSE
 				LET msg = "RootContainer aligns by the client's window. Can be resized with client's window resizing"
 			END IF
 			CALL OpenWindowWithForm("str_str",msg)
	 	ON ACTION "form3"
			IF isBorder THEN
 				LET msg = "RootContainer aligns by size defined in form 300x200px and located in left-top side of the client's window in dialog window. RootContainer is not resizable"
 			ELSE
 				LET msg = "RootContainer aligns by size defined in form 300x200px and located in left-top side of the client's window. RootContainer is not resizable."
 			END IF
 			CALL OpenWindowWithForm("left_top_pref",msg) 	
 		ON ACTION "form4"
			IF isBorder THEN
 				LET msg = "RootContainer aligns by child content and locates in dialog window. Child Label has size 300x200px defined in the form. RootContainer is not resizable."
 			ELSE
 				LET msg = "Rootcontainer aligns by child content and locates in Top-Left corner of the client's window. RootContainer is not resizable."
 			END IF
	 		CALL OpenWindowWithForm("left_top_child_pref",msg)
 		ON ACTION "form5"
			IF isBorder THEN
 				LET msg = "RootContainer aligns by its child(Gridpanel).\nChild(Gridpanel) doesn't have size.\nRootcontainer collapses to 0x0px to the Left-Top corner in dialog window."
 			ELSE
 				LET msg = "RootContainer aligns by its child(Gridpanel).\nChild(Gridpanel) doesn't have size.\nRootcontainer collapses to 0x0px to the Left-Top corner in client's window."
 			END IF
	 		CALL OpenWindowWithForm("left_top_child_strch",msg)
	END INPUT

END MAIN

FUNCTION OpenWindowWithForm(form_name,msg)
DEFINE form_name CHAR(50)
DEFINE msg STRING
	IF isBorder THEN
    	OPEN WINDOW w WITH FORM form_name ATTRIBUTE(BORDER)
    ELSE
    	OPEN WINDOW w WITH FORM form_name
    END IF
    MENU
    	ON ACTION Info
    		CALL fgl_winmessage("Info",msg,"info")	
    	ON ACTION Accept
    		EXIT MENU
    END MENU
    CLOSE WINDOW w
END FUNCTION