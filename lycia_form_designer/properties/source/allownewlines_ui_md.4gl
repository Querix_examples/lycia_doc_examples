##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lb1, lb2 STRING
DEFINE lb_1, lb_2 ui.Label
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "allownewlines_ui_md" ATTRIBUTE(BORDER)
LET lb_1 = ui.Label.ForName("lb1")
LET lb_2 = ui.Label.ForName("lb2")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("change" ,"change" , "qx://application/create.svg")
    CALL fgl_setactionlabel("view" ,"view" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")  
  ON ACTION "change"
    CALL lb_1.setAllowNewLines(0)
    CALL lb_2.setAllowNewLines(1)
    CALL lb_1.setText("allowNewLines enables multiple-line display")
    CALL lb_2.setText("allowNewLines enables multiple-line display")
  ON ACTION "view"
    LET tmp = "With lb1, allowNewLines = ", lb_1.getAllowNewLines(), "\n",
              "With lb2, allowNewLines = ", lb_2.getAllowNewLines()
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN