##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, dspl STRING
DEFINE tb ui.Toolbar

OPEN WINDOW w WITH FORM "hidelabels" ATTRIBUTE(BORDER)
LET tb = ui.Toolbar.ForName("toolbarMain1")

LET dspl = "hideLabels=", tb.getHideLabels()
DISPLAY dspl TO f1

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("act1" ,"act1" , "qx://application/biuld.svg")
    CALL fgl_setactionlabel("act2" ,"act2" , "qx://application/clear.svg")
  ON ACTION "act1"
    CALL tb.SetHideLabels(1)
    LET dspl = "hideLabels=", tb.getHideLabels()
    DISPLAY dspl TO f1
  ON ACTION "act2"
    EXIT MENU
END MENU 

END MAIN