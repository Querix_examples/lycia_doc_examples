##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2, f3 STRING

OPEN WINDOW w WITH FORM "tocase_01" ATTRIBUTE(BORDER)

INPUT BY NAME f1 WITHOUT DEFAULTS
INPUT BY NAME f2 WITHOUT DEFAULTS
INPUT BY NAME f3 WITHOUT DEFAULTS

DISPLAY f1
DISPLAY f2
DISPLAY f3

CALL fgl_getkey()

END MAIN