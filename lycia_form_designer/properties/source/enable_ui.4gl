##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN 
DEFINE bt_ui ui.Button
DEFINE tf_ui ui.TextField

OPEN WINDOW w1 WITH FORM "enable_ui" ATTRIBUTE(BORDER)

LET bt_ui = ui.Button.forName("bt1")
CALL bt_ui.setEnable(TRUE)

LET tf_ui = ui.TextField.forName("tf1")
CALL tf_ui.setEnable(TRUE)    

CALL fgl_getkey()
END MAIN  