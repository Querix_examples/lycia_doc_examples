##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f_calendar ui.Calendar

OPEN WINDOW w WITH FORM "text_calendar" ATTRIBUTE(BORDER)
LET f_calendar = ui.Calendar.ForName("f1")
CALL f_calendar.SetEnable(TRUE)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("ui.SetText" ,"ui.SetText" , "qx://application/create.svg")
    CALL fgl_setactionlabel("DISPLAY...TO" ,"DISPLAY...TO" , "qx://application/brush.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  COMMAND "ui.SetText"
    CALL f_calendar.SetText("ui.SetText")
  COMMAND "DISPLAY...TO"
    DISPLAY "DISPLAY...TO" to f1
  COMMAND "exit"
    EXIT MENU
END MENU

CALL fgl_getkey()
END MAIN