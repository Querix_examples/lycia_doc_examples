##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lb1, lb2 STRING
 
OPEN WINDOW w WITH FORM "isdynamic_md" ATTRIBUTE(BORDER)
 
MENU
  BEFORE MENU
    CALL fgl_setactionlabel("1st display" ,"1st display" , "qx://application/one.svg")
    CALL fgl_setactionlabel("2nd display" ,"2nd display" , "qx://application/two.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "1st display"
    DISPLAY "1st display" TO lb1 
    DISPLAY "1st display" TO lb2 
  ON ACTION "2nd display"
    DISPLAY "2nd display" TO lb1
    DISPLAY "qx://application/two.svg" TO lb1 
    DISPLAY "2nd display" TO lb2
    DISPLAY "qx://application/two.svg" TO lb2 
  ON ACTION "exit"
    EXIT MENU
 END MENU
END MAIN