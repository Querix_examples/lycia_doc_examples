##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1 STRING
DEFINE f2 DECIMAL(16,2)
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "format_vs_textpicture" ATTRIBUTE(BORDER)

CALL fgl_winmessage("", "Please, enter any 5 digits.", "info")
INPUT BY NAME f1, f2 WITHOUT DEFAULTS

LET tmp = "1st input", "\n",
          "data type: ", "STRING", "\n",
          "received value: ", f1, "\n",
          "2nd input", "\n",
          "data type: ", "DECIMAL(16,2)", "\n",
          "received value: ", trim(f2)
CALL fgl_winmessage("", tmp, "")

END MAIN