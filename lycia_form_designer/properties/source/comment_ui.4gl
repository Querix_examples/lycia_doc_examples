##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1 STRING
DEFINE tf ui.TextField

OPEN WINDOW w WITH FORM "comment_ui" ATTRIBUTE(BORDER, STYLE="panels1")

LET tf = ui.TextField.Forname("f1")
CALL tf.setComment("Use setComment() to set comments.")

INPUT BY NAME f1
END MAIN