##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE cmb ui.ComboBox
DEFINE ta ui.TextArea
DEFINE calendar ui.Calendar
DEFINE tmp1, tmp2, tmp3 STRING
DEFINE cmb_htxt, cmb_ltxt STRING
DEFINE ta_htxt, ta_ltxt, ta_pltxt STRING
DEFINE c_htxt, c_ltxt STRING
DEFINE choose STRING

OPEN WINDOW w WITH FORM "helper_label_placeholder_texts_ui" ATTRIBUTE(BORDER)
LET cmb = ui.ComboBox.ForName("f1")
LET ta = ui.TextArea.ForName("f2")
LET calendar = ui.Calendar.ForName("f3")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/create.svg")
    CALL fgl_setactionlabel("display" ,"display" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "set"
    LET choose = UPSHIFT(fgl_winbutton("", "What widget do you want to edit?", "ComboBox", "ComboBox|TextArea|Calendar", "", 1))
    CASE choose
      WHEN "COMBOBOX"
        LET cmb_htxt = fgl_winprompt(5, 3, "Enter the helper text.", "", 250, 0)
          CALL cmb.SetHelperText(cmb_htxt)
        LET cmb_ltxt = fgl_winprompt(5, 3, "Enter the label text.", "", 250, 0)
          CALL cmb.SetLabelText(cmb_ltxt)
      WHEN "TEXTAREA"
        LET ta_htxt = fgl_winprompt(5, 3, "Enter the helper text.", "", 250, 0)
          CALL ta.SetHelperText(ta_htxt)
        LET ta_ltxt = fgl_winprompt(5, 3, "Enter the placeholder text.", "", 250, 0)
          CALL ta.SetPlaceholderText(ta_ltxt)
        LET ta_ltxt = fgl_winprompt(5, 3, "Enter the label text.", "", 250, 0)
          CALL ta.SetLabelText(ta_ltxt)
      WHEN "CALENDAR"
        LET c_htxt = fgl_winprompt(5, 3, "Enter the helper text.", "", 250, 0)
          CALL cmb.SetHelperText(c_htxt)
        LET c_ltxt = fgl_winprompt(5, 3, "Enter the label text.", "", 250, 0)
          CALL cmb.SetLabelText(c_ltxt)
    END CASE
  ON ACTION "display"
      LET tmp1 = "For ComboBox: \n",
                "helperText: ", cmb.GetHelperText(cmb), "\n",
                "labelText: ", cmb.GetLabelText(cmb)
      LET tmp2 = "For TextArea: \n",
                "helperText: ", ta.GetHelperText(ta), "\n",
                "labelText: ", ta.GetLabelText(ta), "\n",
                "placeholderText: ", ta.GetPlaceholderText(ta)
      LET tmp3 = "For Calendar: \n",
                "helperText: ", calendar.GetHelperText(calendar), "\n",
                "labelText: ", calendar.GetLabelText(calendar)
      CALL fgl_winmessage("", tmp1, "")
      CALL fgl_winmessage("", tmp2, "")
      CALL fgl_winmessage("", tmp3, "")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN

