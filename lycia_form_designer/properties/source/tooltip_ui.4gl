##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE bt1, f2, dspl STRING,
       bt ui.Button

OPEN WINDOW w WITH FORM "tooltip_ui" ATTRIBUTE(BORDER)
LET bt = ui.Button.ForName("bt1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/search.svg")
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/create.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  COMMAND "get"
    LET dspl = "Tooltip text - ", bt.GetTooltip()
    DISPLAY dspl TO f2
  COMMAND "set"
    CALL bt.SetTooltip("My new tooltip")
    LET dspl = "Tooltip text - ", bt.GetTooltip()
    DISPLAY dspl TO f2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN