##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING
DEFINE tf1, tf2 ui.TextField

OPEN WINDOW w WITH FORM "reverse_ui" ATTRIBUTE(BORDER)
LET tf1 = ui.TextField.ForName("f1")
LET tf2 = ui.TextField.ForName("f2")
CALL tf1.SetReverse(0)
CALL tf2.SetReverse(1)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display" ,"display" , "qx://application/search.svg")
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "display"
    DISPLAY "text" TO f1
    DISPLAY "text" TO f2
  ON ACTION "input"
    INPUT BY NAME f1, f2;
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN