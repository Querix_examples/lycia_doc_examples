##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2, f3, f4, f5, f6 DATE
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "format_date" ATTRIBUTE(BORDER)
INPUT BY NAME f1, f2, f3, f4, f5, f6

LET tmp = "Compare: ", "\n",
          f1, "\n",
          f2, "\n",
          f3, "\n",
          f4, "\n",
          f5, "\n",
          f6
CALL fgl_winmessage("", tmp, "")

END MAIN