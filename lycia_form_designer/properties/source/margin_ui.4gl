##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE bt ui.Button
DEFINE marg ui.Thickness
DEFINE l, r, t, b INTEGER
DEFINE input_var, tmp STRING

OPEN WINDOW w_main WITH FORM "margin_ui_main" ATTRIBUTE(BORDER)
LET bt = ui.Button.ForName("bt1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("change", "change", "qx://application/create.svg")
    CALL fgl_setactionlabel("display", "display", "qx://application/search.svg")
    CALL fgl_setactionlabel("exit", "exit", "qx://application/clear.svg")
  ON ACTION "change"
        OPEN WINDOW w_in WITH FORM "margin_ui_input" ATTRIBUTE(BORDER)
        INPUT BY NAME l, t, r, b
        CLOSE WINDOW w_in
        LET marg.Left = l
        LET marg.Right = r
        LET marg.Top = t
        LET marg.Bottom = b
        CALL bt.SetMargin(marg)
  ON ACTION "display"
    LET tmp = "margin: ", "\n",
              bt.getMargin()
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN