##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lbx ui.ListBox

OPEN WINDOW w WITH FORM "enablemultiselection_ui" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("lbx1")

MENU
	BEFORE MENU
    CALL fgl_setactionlabel("check" ,"check" , "qx://application/search.svg")
    CALL fgl_setactionlabel("change" ,"change" , "qx://application/create.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "check"
    DISPLAY "enableMultiSelection = ", lbx.getEnableMultiSelection()
  ON ACTION "change"
  	IF UPSHIFT(fgl_winquestion("", "Do you want to enable multiselection?", "Yes", "Yes|No", "question", 1))="YES" THEN
		CALL lbx.setEnableMultiSelection(1)
		DISPLAY "enableMultiSelection = ", lbx.getEnableMultiSelection()
  	ELSE 
        CALL lbx.setEnableMultiSelection(0)
        DISPLAY "enableMultiSelection = ", lbx.getEnableMultiSelection()
  	END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN