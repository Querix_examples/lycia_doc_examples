##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE tbb_one, tbb_two ui.ToolbarButton

OPEN WINDOW w WITH FORM "place_ui" ATTRIBUTE(BORDER)

LET tbb_one = ui.ToolbarButton.ForName("tbb1")
LET tbb_two = ui.ToolbarButton.ForName("tbb2")

CALL tbb_one.setPlace("top")

MENU
	ON ACTION act1
		DISPLAY "With tbb1, place = ", tbb_one.getPlace()
	ON ACTION act2
		DISPLAY "With tbb2, place = ", tbb_two.getPlace()
	ON ACTION exit
		EXIT MENU
END MENU

END MAIN