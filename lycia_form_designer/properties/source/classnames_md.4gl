##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lb1, lb2 ui.Label
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "classnames_md" ATTRIBUTE(BORDER)

LET lb1 = ui.Label.ForName("lb1")
LET lb2 = ui.Label.ForName("lb2")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/create.svg")
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
  ON ACTION "get"
    LET tmp = "classNames for lb1: ", lb1.GetClassNames(), "\n",
              "classNames for lb2: ", lb2.GetClassNames()
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "set"
    CALL lb1.SetClassNames("first_class")
    CALL lb2.SetClassNames("second_class")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN