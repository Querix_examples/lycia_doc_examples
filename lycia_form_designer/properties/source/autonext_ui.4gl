##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1 STRING
DEFINE ta ui.TextArea

OPEN WINDOW w WITH FORM "autonext_ui" ATTRIBUTE(BORDER)

LET ta = ui.TextArea.ForName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/build.svg")
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
	COMMAND "set"
		CALL ta.setAutonext(1)
	COMMAND "input"
		INPUT BY NAME f1 WITHOUT DEFAULTS
	COMMAND "get"
		DISPLAY "autonext is set to ", ta.getAutonext(), "."
	COMMAND "exit"
		EXIT MENU
END MENU 

END MAIN