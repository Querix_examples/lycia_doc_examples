##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE grid ui.GridPanel
DEFINE rLocation ui.GridItemLocation
DEFINE lb ui.Label
DEFINE tmp STRING

OPEN WINDOW w_main WITH FORM "griditemlocation_ui_basic" ATTRIBUTE(BORDER)
LET grid = ui.GridPanel.ForName("rootContainer")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("create", "create", "qx://application/create.svg")
    CALL fgl_setactionlabel("display", "display", "qx://application/search.svg")
    CALL fgl_setactionlabel("exit", "exit", "qx://application/clear.svg")
  ON ACTION "create"
        LET rLocation.GridX = 0
        LET rLocation.GridY = 2
        LET rLocation.GridHeight = 1
        LET rLocation.GridWidth = 2
        LET lb = ui.Label.Create("l1", "rootContainer")
        CALL lb.SetGridItemLocation(rLocation)
        CALL lb.SetText("LABEL")
  ON ACTION "display"
    LET tmp = lb.getGridItemLocation()
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN