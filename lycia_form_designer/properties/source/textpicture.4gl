##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13 CHAR(4)
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "textPicture" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display", "display", "qx://application/search.svg")
    CALL fgl_setactionlabel("input", "input", "qx://application/create.svg")
    CALL fgl_setactionlabel("exit", "exit", "qx://application/clear.svg")
  ON ACTION "display"
    DISPLAY "1234" to f1
    DISPLAY "1234" to f2
    DISPLAY "1234" to f3
    DISPLAY "ABCD" to f4
    DISPLAY "ABCD" to f5
    DISPLAY "ABCD" to f6
    DISPLAY "ABCD" to f7
    DISPLAY "1234" to f8
    DISPLAY "1234" to f9
    DISPLAY "1234" to f10
    DISPLAY "1A2B" to f11
    DISPLAY "AB34" to f12
    DISPLAY "12CD" to f13
  ON ACTION "input"
    INPUT BY NAME f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13 WITHOUT DEFAULTS
    LET tmp = f1, "\n",
              f2, " | ", f3, " | ", f4, "\n",
              f5, " | ", f6, " | ", f7, "\n",
              f8, " | ", f9, " | ", f10, "\n",
              f11, " | ", f12, " | ", f13
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN