##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1,f2,f3,f4 DATE
DEFINE tmp STRING
 
OPEN WINDOW w1 WITH FORM "century_md" ATTRIBUTE(BORDER)

INPUT BY NAME f1,f2,f3,f4
LET tmp = f1, "\n",
          f2, "\n",
          f3, "\n",
          f4
CALL fgl_winmessage("", tmp, "")
 
END MAIN