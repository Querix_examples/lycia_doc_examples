##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE grid ui.GridPanel
DEFINE rLocation ui.GridItemLocation
DEFINE w, h, x, y INTEGER
DEFINE lb ui.Label
DEFINE input_var, tmp STRING

OPEN WINDOW w_main WITH FORM "gil_adv_main" ATTRIBUTE(BORDER)
LET grid = ui.GridPanel.ForName("rootContainer")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("create", "create", "qx://application/create.svg")
    CALL fgl_setactionlabel("display", "display", "qx://application/search.svg")
    CALL fgl_setactionlabel("exit", "exit", "qx://application/clear.svg")
  ON ACTION "create"
    IF UPSHIFT(fgl_winquestion("", "Are you familiar with gridItemLocation?", "Yes", "Yes|No", "question", 1))="YES" THEN
        OPEN WINDOW w_in WITH FORM "gil_adv_input" ATTRIBUTE(BORDER)
        INPUT BY NAME x, y, h, w
        CLOSE WINDOW w_in
        LET rLocation.GridX = x
        LET rLocation.GridY = y
        LET rLocation.GridHeight = h
        LET rLocation.GridWidth = w
        IF x < 4 AND y < 4 AND h < 5 AND w < 5 THEN
          LET lb = ui.Label.Create("l1", "rootContainer")
          CALL lb.SetHorizontalAlignment("Stretch")
          CALL lb.SetVerticalAlignment("Stretch")
          CALL lb.SetGridItemLocation(rLocation)
          CALL lb.SetText("LABEL")
        ELSE
          CALL fgl_winmessage("Sorry!", "This forms doesn't have enough columns/rows to place this label where you have specified.", "stop")
        END IF
  	ELSE 
        OPEN WINDOW w_info AT 5,5 WITH FORM "gil_adv_info" ATTRIBUTE(BORDER)
        CALL fgl_getkey()
        CLOSE WINDOW w_info
        OPEN WINDOW w_in WITH FORM "gil_adv_input" ATTRIBUTE(BORDER)
        INPUT BY NAME x, y, h, w
        CLOSE WINDOW w_in
        LET rLocation.GridX = x
        LET rLocation.GridY = y
        LET rLocation.GridHeight = h
        LET rLocation.GridWidth = w
        IF x < 4 AND y < 4 AND h < 5 AND w < 5 THEN
          LET lb = ui.Label.Create("l1", "rootContainer")
          CALL lb.SetHorizontalAlignment("Stretch")
          CALL lb.SetVerticalAlignment("Stretch")
          CALL lb.SetGridItemLocation(rLocation)
          CALL lb.SetText("LABEL")
        ELSE
          CALL fgl_winmessage("", "This forms doesn't have enough columns/row to place this label where you have specified.", "stop")
        END IF
    END IF
  ON ACTION "display"
    LET tmp = lb.getGridItemLocation()
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN