##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING
DEFINE ta1, ta2 ui.TextArea
DEFINE choose, autonext, maxlength, htxt STRING
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "autonext_complex" ATTRIBUTE(BORDER)
LET ta1 = ui.TextArea.ForName("f1")
LET ta2 = ui.TextArea.ForName("f2")


MENU
  BEFORE MENU
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/settings.svg")
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("view" ,"view" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
  COMMAND "set"
    LET choose = UPSHIFT(fgl_winbutton("", "What widget do you want to edit?", "ta1", "ta1|ta2", "", 1))
    CASE choose
      WHEN "TA1"
        LET autonext = UPSHIFT(fgl_winbutton("", "Do you want to move to the next field after max length?", "yes", "yes|no", "", 1))
          CASE autonext
            WHEN "YES"
              CALL ta1.SetAutonext(1)
            WHEN "NO"
              CALL ta1.SetAutonext(0)
          END CASE
        LET maxlength = fgl_winprompt(5, 3, "Enter the necessary max length.", "", 250, 0)
          CALL ta1.SetMaxLength(maxlength)
          LET htxt = "New max length is ", maxlength, " characters."
          CALL ta1.SetHelperText(htxt)
      WHEN "TA2"
        LET autonext = UPSHIFT(fgl_winbutton("", "Do you want to move to the next field after max length?", "yes", "yes|no", "", 1))
          CASE autonext
            WHEN "YES"
              CALL ta2.SetAutonext(1)
            WHEN "NO"
              CALL ta2.SetAutonext(0)
          END CASE
        LET maxlength = fgl_winprompt(5, 3, "Enter the necessary max length.", "", 250, 0)
          CALL ta2.SetMaxLength(maxlength)
          LET htxt = "New max length is ", maxlength, " characters."
          CALL ta2.SetHelperText(htxt)
    END CASE
  COMMAND "input"  
    INPUT BY NAME f1 WITHOUT DEFAULTS
    INPUT BY NAME f2 WITHOUT DEFAULTS
  COMMAND "view"
    LET tmp = "With f1, autonext = ", ta1.getAutonext(), "\n",
              "With f1, maxLength = ", trim(ta1.getMaxLength()), "\n",
              "With f2, autonext = ", ta2.getAutonext(), "\n",
              "With f2, maxLength = ", trim(ta2.getMaxLength())
    CALL fgl_winmessage("", tmp, "")
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN