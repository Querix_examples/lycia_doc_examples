##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
 DEFINE lb ui.Label
 
 OPEN WINDOW w WITH FORM "isdynamic_ui" ATTRIBUTE(BORDER)
 
 LET lb = ui.Label.ForName("lb1")

 MENU
 ON ACTION "act1"
   CALL lb.SetIsDynamic(0)
   DISPLAY lb.GetIsDynamic()
   DISPLAY "not dynamic" TO lb1
 ON ACTION "act2"
   CALL lb.SetIsDynamic(1)
   DISPLAY lb.GetIsDynamic()
   DISPLAY "dynamic" TO lb1
 COMMAND "EXIT"
   EXIT MENU
 END MENU
END MAIN