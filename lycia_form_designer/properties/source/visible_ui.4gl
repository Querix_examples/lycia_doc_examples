##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE bt1, bt2 STRING,
       bt_1, bt_2 ui.Button

OPEN WINDOW w WITH FORM "visible_ui" ATTRIBUTE(BORDER)

LET bt_1 = ui.Button.ForName("bt1")
LET bt_2 = ui.Button.ForName("bt2")

CALL bt_1.setVisible(0)
CALL bt_2.setVisible(1)

DISPLAY "With bt1, setVisible=", bt_1.getVisible(), "."
DISPLAY "With bt2, setVisible=", bt_2.getVisible(), "."

CALL fgl_getkey()

END MAIN