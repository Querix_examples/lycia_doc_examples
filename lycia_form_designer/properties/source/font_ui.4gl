##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lb1, lb2, lb3 ui.Label
DEFINE fnt1, fnt2, fnt3 ui.Font

OPEN WINDOW w WITH FORM "font_ui" ATTRIBUTE(BORDER)
LET lb1 = ui.Label.ForName("lb1")
LET lb2 = ui.Label.ForName("lb2")
LET lb3 = ui.Label.ForName("lb3")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("Test ui" ,"Test ui" , "qx://application/create.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  COMMAND "Test ui"
    LET fnt1.Family = ["Cambria"]
    LET fnt1.Bold = TRUE
    LET fnt1.Italic = FALSE
    LET fnt1.Underline = FALSE
    LET fnt1.FontSize = 25
    CALL lb1.SetFont(fnt1)

    LET fnt2.Family = ["Times New Roman"]
    LET fnt2.Bold = TRUE
    LET fnt2.Italic = FALSE
    LET fnt2.Underline = TRUE
    LET fnt2.FontSize = 20
    CALL lb2.SetFont(fnt2)

    LET fnt3.Family = ["Bookman Old Style"]
    LET fnt3.Bold = FALSE
    LET fnt3.Italic = TRUE
    LET fnt3.Underline = FALSE
    LET fnt3.FontSize = 35
    CALL lb3.SetFont(fnt3)
    COMMAND "Exit"
      EXIT MENU
  END MENU

END MAIN