##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lb1 ui.Label
DEFINE bg ui.Background
DEFINE sc ui.SystemColor

OPEN WINDOW w WITH FORM "background_ui" ATTRIBUTE(BORDER)
LET lb1 = ui.Label.ForName("lb1")

#BackgroundImage
CALL bg.SetBackgroundImage("qx://application/querix.png")

#BackgroundStyle
CALL bg.SetBackgroundStyle("Centered")
--CALL bg.SetBackgroundStyle("Normal")

#FillColor
CALL sc.SetSystemColorName("LightCyan")
CALL bg.SetFillColor(sc)

#Size
--CALL bg.SetSize(["50px","50px"])

#Location
--CALL bg.SetLocation(["175px","100px"])

CALL lb1.SetBackground(bg)

CALL fgl_getkey()
END MAIN