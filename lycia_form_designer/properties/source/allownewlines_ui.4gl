##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lb1, lb2 STRING,
       lb_1, lb_2 ui.Label

OPEN WINDOW w WITH FORM "allownewlines_ui" ATTRIBUTE(BORDER)
LET lb_1 = ui.Label.ForName("lb1")
LET lb_2 = ui.Label.ForName("lb2")

CALL lb_1.setAllowNewLines(0)
CALL lb_2.setAllowNewLines(1)
CALL lb_1.setText("allowNewLines enables multiple-line display")
CALL lb_2.setText("allowNewLines enables multiple-line display")
DISPLAY "With lb1, allowNewLines=", lb_1.getAllowNewLines()
DISPLAY "With lb2, allowNewLines=", lb_2.getAllowNewLines()

CALL fgl_getkey()
END MAIN