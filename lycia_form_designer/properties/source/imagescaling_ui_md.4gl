##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lb ui.Label
DEFINE image_ui ui.Image
DEFINE scaling CHAR(16)
DEFINE tmp STRING

OPEN WINDOW n WITH FORM "imagescaling_ui_md" ATTRIBUTE(BORDER) 

LET lb = ui.Label.forName("lb1")

CALL image_ui.setImageUrl("qx://application/large_lycia.png")
CALL image_ui.setImageScaling("None")
CALL lb.SetImage(image_ui)
LET scaling = image_ui.getImageScaling()
    
MENU
  BEFORE MENU
    CALL fgl_setactionlabel("change" ,"change" , "qx://application/image.svg")
    CALL fgl_setactionlabel("display" ,"display" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "change"
    CASE scaling
      WHEN "None"
        CALL image_ui.setImageScaling("Vertical")
        CALL lb.SetImage(image_ui)
        LET scaling = image_ui.getImageScaling()

      WHEN "Vertical"
        CALL image_ui.setImageScaling("Horizontal")
        CALL lb.SetImage(image_ui)
        LET scaling = image_ui.getImageScaling()
  
      WHEN "Horizontal"
        CALL image_ui.setImageScaling("Both")
        CALL lb.SetImage(image_ui)
        LET scaling = image_ui.getImageScaling()
  
      WHEN "Both"
        CALL image_ui.setImageScaling("None")
        CALL lb.SetImage(image_ui)
        LET scaling = image_ui.getImageScaling()
    END CASE
  
  ON ACTION "display"
      LET tmp = "ImageScaling: ", scaling
      CALL fgl_winmessage("", tmp, "")
  
  ON ACTION "exit"
    EXIT MENU
  END MENU

END MAIN