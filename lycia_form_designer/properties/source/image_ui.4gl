##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE btn ui.Button
DEFINE img ui.Image

OPEN WINDOW w WITH FORM "image_ui" ATTRIBUTE(BORDER)
LET btn = ui.Button.forName("bt1")

CALL img.setImageUrl("qx://application/middle_lycia.png")
--CALL img.setImageScaling("Both")
CALL img.setImagePosition("Top")
--CALL img.SetSize(["50px","50px"])

CALL btn.SetImage(img)
CALL btn.SetText("Text")

CALL fgl_getkey()
END MAIN