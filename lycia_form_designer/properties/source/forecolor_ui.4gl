##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


MAIN
DEFINE lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9 ui.Label
DEFINE sc_green, sc_red, sc_blue ui.SystemColor
DEFINE sc_lgreen, sc_purple, sc_orange, sc_steelblue ui.SystemColor
DEFINE cc01, cc02, cc03 ui.CustomizedColor

OPEN WINDOW w WITH FORM "forecolor_ui" ATTRIBUTE(BORDER)

LET lb1 = ui.Label.ForName("lb1")
LET lb2 = ui.Label.ForName("lb2")
LET lb3 = ui.Label.ForName("lb3")
LET lb4 = ui.Label.ForName("lb4")
LET lb5 = ui.Label.ForName("lb5")
LET lb6 = ui.Label.ForName("lb6")
LET lb7 = ui.Label.ForName("lb7")
LET lb8 = ui.Label.ForName("lb8")
LET lb9 = ui.Label.ForName("lb9")

CALL sc_green.SetSystemColorName("Green")
CALL lb1.SetForeColor(sc_green)

CALL sc_red.SetSystemColorName("Red")
CALL lb2.SetForeColor(sc_red)

CALL sc_blue.SetSystemColorName("Blue")
CALL lb3.SetForeColor(sc_blue)

CALL sc_lgreen.SetSystemColorName("LightGreen")
CALL lb4.SetForeColor(sc_lgreen)

CALL sc_orange.SetSystemColorName("Orange")
CALL lb5.SetForeColor(sc_orange)

CALL sc_purple.SetSystemColorName("Purple")
CALL lb6.SetForeColor(sc_purple)

LET cc01 =  ui.CustomizedColor.Create("cc01")
LET cc02 =  ui.CustomizedColor.Create("cc02")
LET cc03 =  ui.CustomizedColor.Create("cc03")

--00FF00 = lime
CALL cc01.SetAlpha(255)
CALL cc01.SetRedColor(0)
CALL cc01.SetGreenColor(255)
CALL cc01.SetBlueColor(0)

--FFD700 = gold
CALL cc02.SetAlpha(255)
CALL cc02.SetRedColor(255)
CALL cc02.SetGreenColor(215)
CALL cc02.SetBlueColor(0)

--0099CF = docs blue
CALL cc03.SetAlpha(255)
CALL cc03.SetRedColor(0)
CALL cc03.SetGreenColor(153)
CALL cc03.SetBlueColor(207)


CALL lb7.SetForeColor(cc01)
CALL lb8.SetForeColor(cc02)
CALL lb9.SetForeColor(cc03)

CALL fgl_getkey()
END MAIN