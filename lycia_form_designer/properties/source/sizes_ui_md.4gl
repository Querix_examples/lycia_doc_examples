##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE grd ui.GridPanel
DEFINE prsize, maxsize, minsize ui.Size
DEFINE tmp STRING
DEFINE choose STRING

OPEN WINDOW w WITH FORM "sizes_ui_md" ATTRIBUTE(BORDER)
LET grd = ui.GridPanel.ForName("rootContainer")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/settings.svg")
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
  ON ACTION "get"
    LET tmp = "preferredSize = ", grd.getPreferredSize(), "\n",
              "minSize = ", grd.getMinSize(), "\n",
              "maxSize = ", grd.getMaxSize()
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "set"
    LET choose = UPSHIFT(fgl_winbutton("", "What do you want to change?", "preferredSize", "preferredSize|minSize|maxSize", "", 1))
    CASE choose
      WHEN "PREFERREDSIZE"
        LET prsize.width = fgl_winprompt(5, 3, "Enter the necessary width (in pixels).", "", 250, 0)
        LET prsize.height = fgl_winprompt(5, 3, "Enter the necessary height in (pixels).", "", 250, 0)
        CALL grd.setPreferredSize(prsize)
      WHEN "MINSIZE"
        LET minsize.width = fgl_winprompt(5, 3, "Enter the necessary width (in pixels).", "", 250, 0)
        LET minsize.height = fgl_winprompt(5, 3, "Enter the necessary height in (pixels).", "", 250, 0)
        CALL grd.setMinSize(minsize)
      WHEN "MAXSIZE"
        LET maxsize.width = fgl_winprompt(5, 3, "Enter the necessary width (in pixels).", "", 250, 0)
        LET maxsize.height = fgl_winprompt(5, 3, "Enter the necessary height in (pixels).", "", 250, 0)
        CALL grd.setMaxSize(maxsize)
    END CASE
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN