##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE bt_1, bt_2, bt_3, bt_4 ui.Button
DEFINE choose STRING
DEFINE get_bt_1, get_bt_2, get_bt_3, get_bt_4 BOOLEAN

OPEN WINDOW w WITH FORM "collapsed_ui" ATTRIBUTE(BORDER)
LET bt_1 = ui.Button.ForName("bt1")
LET bt_2 = ui.Button.ForName("bt2")
LET bt_3 = ui.Button.ForName("bt3")
LET bt_4 = ui.Button.ForName("bt4")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("test", "collapse", "qx://application/call_received.svg")
    CALL fgl_setactionlabel("restore", "restore", "qx://application/call_made.svg")
    CALL fgl_setactionlabel("exit", "exit", "qx://application/clear.svg")
  ON ACTION "test"
    LET choose = fgl_winquestion("", "Which button do you want to make collapsed?", "1", "1|2|3|4", "question", 1)
    CASE choose
      WHEN 1
        CALL bt_1.SetCollapsed(1)
      WHEN 2
        CALL bt_2.SetCollapsed(1)
      WHEN 3
        CALL bt_3.SetCollapsed(1)
      WHEN 4
        CALL bt_4.SetCollapsed(1)
    END CASE
  ON ACTION "restore"
    LET get_bt_1 = bt_1.getCollapsed()
    LET get_bt_2 = bt_2.getCollapsed()
    LET get_bt_3 = bt_3.getCollapsed()
    LET get_bt_4 = bt_4.getCollapsed()
    IF get_bt_1 = 1 THEN
      CALL bt_1.SetCollapsed(0)
    END IF
    IF get_bt_2 = 1 THEN
      CALL bt_2.SetCollapsed(0)
    END IF
    IF get_bt_3 = 1 THEN
      CALL bt_3.SetCollapsed(0)
    END IF
    IF get_bt_4 = 1 THEN
      CALL bt_4.SetCollapsed(0)
    END IF
  ON ACTION "exit"
    EXIT MENU
END MENU
END MAIN