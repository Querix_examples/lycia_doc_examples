##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE tf1, tf2 ui.TextField
DEFINE set_tf1, set_tf2 STRING
DEFINE f1, f2 STRING
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "ispasswordmask_md" ATTRIBUTE(BORDER)
LET tf1 = ui.TextField.ForName("f1")
LET tf2 = ui.TextField.ForName("f2")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("change" ,"change" , "qx://application/settings.svg")
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")    
  COMMAND "change"
    LET set_tf1 = UPSHIFT(fgl_winbutton("", "Do you want to mask the input for tf1?", "yes", "yes|no", "", 1))
    CASE set_tf1
      WHEN "YES"
        CALL tf1.SetIsPasswordMask(1)
      WHEN "NO"
        CALL tf1.SetIsPasswordMask(0)
    END CASE
    
    LET set_tf2 = UPSHIFT(fgl_winbutton("", "Do you want to mask the input for tf2?", "yes", "yes|no", "", 1))
    CASE set_tf2
      WHEN "YES"
        CALL tf2.SetIsPasswordMask(1)
      WHEN "NO"
        CALL tf2.SetIsPasswordMask(0)
    END CASE
  COMMAND "input"
    INPUT BY NAME f1 WITHOUT DEFAULTS
    INPUT BY NAME f2 WITHOUT DEFAULTS
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN