##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2, f3 STRING,
       tf_1, tf_2, tf_3 ui.TextField

OPEN WINDOW w WITH FORM "toCase_02" ATTRIBUTE(BORDER)
LET tf_1 = ui.TextField.ForName("f1")
LET tf_2 = ui.TextField.ForName("f2")
LET tf_3 = ui.TextField.ForName("f3")

MENU
	BEFORE MENU
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
	COMMAND "input"
		CALL tf_1.setToCase("Up")
		CALL tf_2.setToCase("Down")
		CALL tf_3.setToCase("None")
		INPUT BY NAME f1 WITHOUT DEFAULTS
		INPUT BY NAME f2 WITHOUT DEFAULTS
		INPUT BY NAME f3 WITHOUT DEFAULTS
	COMMAND "get"
		DISPLAY "With f1, toCase=", tf_1.getToCase()
		DISPLAY "With f2, toCase=", tf_2.getToCase()
		DISPLAY "With f3, toCase=", tf_3.getToCase()
	COMMAND "exit"
		EXIT MENU
END MENU 

END MAIN