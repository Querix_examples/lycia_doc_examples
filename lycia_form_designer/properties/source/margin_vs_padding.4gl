##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "margin_vs_padding" ATTRIBUTE(BORDER)

MENU
  ON ACTION "mar"
    LET tmp = "margin: 0, 25, 0, 0", "\n",
              "padding: 0, 0, 0, 0"
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "pad"
    LET tmp = "margin: 0, 0, 0, 0", "\n",
              "padding: 0, 25, 0, 0"
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "clear"
    EXIT MENU
END MENU
END MAIN