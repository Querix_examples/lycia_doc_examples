##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2, f3 STRING

OPEN WINDOW w WITH FORM "verify_4_password_md" ATTRIBUTE(BORDER, STYLE="panels1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display" ,"display" , "qx://application/search.svg")
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")  
  COMMAND "display"
    DISPLAY "hello" TO f1
    DISPLAY "hello" TO f2
    DISPLAY "hello" TO f3
  COMMAND "input"
    INPUT BY NAME f1, f2, f3
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN