##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w1 WITH FORM "notnull_vs_required_1" ATTRIBUTE(BORDER)
INPUT BY NAME f1
INPUT BY NAME f2
CALL fgl_getkey()
CLOSE WINDOW w1

OPEN WINDOW w2 WITH FORM "notnull_vs_required_2" ATTRIBUTE(BORDER)
INPUT BY NAME f1
INPUT BY NAME f2
CALL fgl_getkey()
CLOSE WINDOW w2

OPEN WINDOW w3 WITH FORM "notnull_vs_required_3" ATTRIBUTE(BORDER)
INPUT BY NAME f1
INPUT BY NAME f2
CALL fgl_getkey()
CLOSE WINDOW w3

OPEN WINDOW w4 WITH FORM "notnull_vs_required_4" ATTRIBUTE(BORDER)
INPUT BY NAME f1
INPUT BY NAME f2
CALL fgl_getkey()
CLOSE WINDOW w4

END MAIN