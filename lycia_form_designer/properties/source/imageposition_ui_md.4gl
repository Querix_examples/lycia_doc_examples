##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE bt_ui ui.Button
DEFINE image_ui ui.Image
DEFINE bt_val CHAR(16)
DEFINE tmp STRING

OPEN WINDOW n WITH FORM "imageposition_ui_md" ATTRIBUTE(BORDER) 

LET bt_ui = ui.Button.forName("bt1")

CALL image_ui.setImageUrl("qx://application/large_lycia.png")
CALL image_ui.setImagePosition("Top")
CALL bt_ui.SetImage(image_ui)
CALL bt_ui.SetText("Text")
LET bt_val = image_ui.getImagePosition()
  
MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display" ,"display" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "act_bt"
    CASE bt_val
      WHEN "Top"
        CALL image_ui.setImagePosition("Bottom")
        CALL bt_ui.SetImage(image_ui)
        CALL bt_ui.SetText("Text")
        LET bt_val = image_ui.getImagePosition()

      WHEN "Bottom"
        CALL image_ui.setImagePosition("Left")
        CALL bt_ui.SetImage(image_ui)
        CALL bt_ui.SetText("Text")
        LET bt_val = image_ui.getImagePosition()
  
      WHEN "Left"
        CALL image_ui.setImagePosition("Right")
        CALL bt_ui.SetImage(image_ui)
        CALL bt_ui.SetText("Text")
        LET bt_val = image_ui.getImagePosition()
  
      WHEN "Right"
        CALL image_ui.setImagePosition("Top")
        CALL bt_ui.SetImage(image_ui)
        CALL bt_ui.SetText("Text")
        LET bt_val = image_ui.getImagePosition()
      END CASE
  
    ON ACTION "display"
      LET tmp = "ImageScaling: ", bt_val
      CALL fgl_winmessage("", tmp, "")
  
    ON ACTION "exit"
      EXIT MENU
  END MENU

END MAIN