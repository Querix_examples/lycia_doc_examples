##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2, f3 DECIMAL(16,2)
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "format_num" ATTRIBUTE(BORDER)

INPUT BY NAME f1, f2, f3 WITHOUT DEFAULTS

LET tmp = "1st input: ", f1, "\n",
          "2nd input:", f2, "\n",
          "3rd input: ", f3
CALL fgl_winmessage("", tmp, "")

END MAIN