##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING,
       ta_1, ta_2 ui.TextArea

OPEN WINDOW w WITH FORM "allowtabulation_ui" ATTRIBUTE(BORDER)
LET ta_1 = ui.TextArea.ForName("f1")
LET ta_2 = ui.TextArea.ForName("f2")

MENU
	BEFORE MENU
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
	COMMAND "input"
  	CALL ta_1.setAllowTabulation(0)
  	CALL ta_2.setAllowTabulation(1)
  	INPUT BY NAME f1 WITHOUT DEFAULTS
  	INPUT BY NAME f2 WITHOUT DEFAULTS
  COMMAND "get"
  	DISPLAY "With f1, allowTabulation=", ta_1.getAllowTabulation()
  	DISPLAY "With f2, allowTabulation=", ta_2.getAllowTabulation()
	COMMAND "exit"
		EXIT MENU
END MENU 

END MAIN