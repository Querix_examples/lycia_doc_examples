##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "maxLength" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/create.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
	COMMAND "input"
		INPUT BY NAME f1 WITHOUT DEFAULTS
		INPUT BY NAME f2 WITHOUT DEFAULTS
	COMMAND "exit"
		EXIT MENU
END MENU

END MAIN