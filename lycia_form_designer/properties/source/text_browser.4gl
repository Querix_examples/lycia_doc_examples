##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE br ui.Browser

OPEN WINDOW w WITH FORM "text_browser" ATTRIBUTE(BORDER)
LET br = ui.Browser.ForName("br1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("ui.SetText" ,"ui.SetText" , "qx://application/create.svg")
    CALL fgl_setactionlabel("DISPLAY...TO" ,"DISPLAY...TO" , "qx://application/brush.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
	COMMAND "ui.SetText"
	  CALL br.setText("https://querix.com/go/lycia/index.htm")
	COMMAND "DISPLAY...TO"
	  DISPLAY "qx://application/wc01" TO br1
	COMMAND "EXIT"
	  EXIT MENU
	END MENU
END MAIN