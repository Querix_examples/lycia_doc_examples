##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE tf ui.TextField
DEFINE f1 STRING

OPEN WINDOW w WITH FORM "textfield_ui_01_label_helper_placeholdertext" ATTRIBUTE(BORDER)
LET tf = ui.TextField.forName("f1")

CALL tf.SetHelperText("helperText")
CALL tf.SetLabelText("labelText")
CALL tf.SetPlaceHolderText("placeholderText")

INPUT BY NAME f1

CALL fgl_getkey()
END MAIN