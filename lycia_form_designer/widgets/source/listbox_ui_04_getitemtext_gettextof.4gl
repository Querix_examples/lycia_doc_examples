##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE lbx ui.ListBox
  DEFINE value_var, index_var STRING
    
OPEN WINDOW w WITH FORM "listbox_ui_04_getitemtext_gettextof" ATTRIBUTE(BORDER)
LET lbx = ui.ListBox.ForName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("getItemText()" ,"getItemText()" , "qx://application/account_box.svg")
    CALL fgl_setactionlabel("getTextOf()" ,"getTextOf()" , "qx://application/account_circle.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "getItemText()"
    MESSAGE "getItemText() needs the option's index."
    LET index_var = fgl_winprompt(5, 3, "Type the option's index (Example: 3)", "", 25, 0)
    IF index_var IS NOT NULL THEN
      DISPLAY "Output for getItemText(): ", lbx.GetItemText(index_var), "."
    END IF
  ON ACTION "getTextOf()"
    MESSAGE "getTextOf() needs the option's value."
    LET value_var = fgl_winprompt(5, 3, "Type the option's value (Example: value3)", "", 25, 0)
    IF value_var IS NOT NULL THEN
      DISPLAY "Output for getTextOf(): ", lbx.GetTextOf(value_var), "."
    END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN