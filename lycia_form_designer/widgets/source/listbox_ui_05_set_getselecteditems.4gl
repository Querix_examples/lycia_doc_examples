##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lbx ui.ListBox
DEFINE i, n INTEGER

OPEN WINDOW w WITH FORM "listbox_ui_05_set_getselecteditems" ATTRIBUTE(BORDER)
LET lbx = ui.ListBox.forName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/arrow_up.svg")
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/arrow_down.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "set"
    CALL lbx.SetSelectedItems([0, 2])
  ON ACTION "get"
    DISPLAY lbx.getSelectedItems()
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN