##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1 STRING
DEFINE lbx ui.ListBox
DEFINE i, n INTEGER

OPEN WINDOW w WITH FORM "listbox_ui_04_set_getlistboxvalues" ATTRIBUTE(BORDER)
LET lbx = ui.ListBox.forName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("get" ,"get" , "qx://application/arrow_up.svg")
    CALL fgl_setactionlabel("set" ,"set" , "qx://application/arrow_down.svg")
    CALL fgl_setactionlabel("check" ,"check" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "get"
    DISPLAY lbx.getListBoxValues()
  ON ACTION "set"
    CALL lbx.SetListBoxValues(["new_text1", "new_text2"])
  ON ACTION "check"
    DISPLAY lbx.GetItemCount()
    LET n = lbx.GetItemCount()
    FOR i = 1 TO n
      DISPLAY "The option's text is ", lbx.GetItemText(i), "."
      DISPLAY "The option's value is ", lbx.GetItemName(i), "."
      DISPLAY ""
    END FOR
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN