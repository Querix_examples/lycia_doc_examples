##########################################################################
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE x,y INT,
       msg CHAR(128)
OPEN WINDOW mywin AT 1,1 WITH FORM "combobox_02_functions" ATTRIBUTE(BORDER)

MENU
  COMMAND "_clear"
    CALL fgl_winprompt(5, 2, "Enter index to start clearing: ", "1",  20, 0) returning x
    CALL fgl_winprompt(5, 2, "Enter  index to finish clearing: ", "1",  20, 0) returning y
    MESSAGE fgl_list_clear("f1", x, y)
  COMMAND "_count"
    MESSAGE "There are ", fgl_list_count("f1"), " items in the list."
  COMMAND "_get"
    CALL fgl_winprompt(5, 2, "Enter index to get option: ", "1",  20, 0) returning x
      IF ( fgl_list_get("f1",x) IS NULL )
        THEN
          LET msg = "Option at index: ", x, " not found."
          ERROR msg
        ELSE
          LET msg = "Option value at index ", x, " is: ",fgl_list_get("f1",x)
          MESSAGE msg
      END IF
  COMMAND "_set"
    CALL fgl_winprompt(5, 2, "Enter index for the option: ", "1",  20, 0) returning x
    CALL fgl_winprompt(5, 2, "Enter value for the option: ", "100",  20, 0) returning y
    LET msg = "Option value at index ",fgl_list_set("f1", x, y)," is set to: ",y
    MESSAGE msg
  COMMAND "_find"
    CALL fgl_winprompt(5, 2, "Enter value to find: ", "1",  20, 0) returning x
    LET y = fgl_list_find("f1", x)
    IF y
      THEN
        LET msg = "Value ", x," has index: ", y
        MESSAGE msg
      ELSE
        LET msg = "Value ", x," is not found."
        ERROR msg
      END IF  
  COMMAND "_insert"
    CALL fgl_winprompt(5, 2, "Enter index to insert option: ", "1",  20, 0) returning x
    CALL fgl_winprompt(5, 2, "Enter value to insert: ", "100",  20, 0) returning y
    LET msg = "Value ", y," was inserted at index: ",fgl_list_insert("f1",x,y)
    MESSAGE msg
  COMMAND "_sort"
    CALL fgl_winprompt(5, 2, "Enter sort order(>1 means ascending): ", "1",  20, 0) returning x
    LET y = fgl_list_sort("f1", x)
    IF x > 1
      THEN MESSAGE "Options are sorted in ascending order"
      ELSE MESSAGE "Options are sorted in descending order"
    END IF
  COMMAND "_restore"
    CALL fgl_list_restore("f1")
    MESSAGE "Options restored to default values"
  COMMAND "EXIT"
    EXIT MENU
END MENU

END MAIN