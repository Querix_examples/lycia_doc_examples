##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


MAIN
OPEN WINDOW w WITH FORM "menucommand" ATTRIBUTE(BORDER)

CALL ui.Interface.setType("container")
CALL ui.Application.GetCurrent().setMenuType("Menu")
CALL ui.Application.GetCurrent().SetClassNames(["md-sidebar"])

MENU
  COMMAND "act1"
    DISPLAY "ONE"
  COMMAND "act2"
    DISPLAY "TWO"
  COMMAND "act_exit"
    EXIT MENU
END MENU

END MAIN