##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE rg ui.RadioGroup
DEFINE r1, r2 ui.Radio

OPEN WINDOW w WITH FORM "radiogroup_ui_02_add_radios" ATTRIBUTE(BORDER)
LET rg = ui.RadioGroup.forName("f1")

LET r1 = ui.Radio.Create("r1")
LET r2 = ui.Radio.Create("r2")

CALL rg.SetRadios([r1, r2])

CALL r1.SetTitle("Hello")
CALL r2.SetTitle("Bye")

CALL fgl_getkey()
END MAIN