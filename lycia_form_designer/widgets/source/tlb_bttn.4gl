##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE lbx STRING

OPEN WINDOW w WITH FORM "tlb_bttn_0" ATTRIBUTE(BORDER)
INPUT BY NAME lbx
	ON CHANGE lbx
		CASE lbx
			WHEN "value1"
				CALL tb_show("tlb_bttn_1")
			WHEN "value2"
				CALL tb_show("tlb_bttn_2")
			WHEN "value3"
				CALL tb_show("tlb_bttn_3")
			WHEN "value4"
				CALL tb_show("tlb_bttn_4")
			WHEN "value5"
				CALL tb_show("tlb_bttn_5")
			WHEN "value6"
				CALL tb_show("tlb_bttn_6")
		END CASE
END INPUT
END MAIN

FUNCTION tb_show(frm)
DEFINE frm STRING
DEFINE w1 ui.Window
CALL w1.openWithForm("w1", frm, 5, 5,"border")
			MENU
			  ON ACTION act
			    EXIT MENU
			END MENU
CALL w1.Close()

END FUNCTION