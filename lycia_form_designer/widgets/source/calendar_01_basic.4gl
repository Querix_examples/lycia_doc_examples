##########################################################################
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE f1 DATETIME YEAR TO DAY

OPEN WINDOW w WITH FORM "calendar_01_basic" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/calendar_today.svg")
  COMMAND "input"
    INPUT BY NAME f1 WITHOUT DEFAULTS
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN