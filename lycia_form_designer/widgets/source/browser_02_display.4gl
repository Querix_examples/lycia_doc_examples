##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE cmb ui.Combobox
DEFINE cmb_opt STRING
DEFINE i, n INTEGER

OPEN WINDOW w WITH FORM "browser_02_display" --ATTRIBUTE(BORDER)
LET cmb = ui.ComboBox.ForName("cmb1")
INPUT cmb FROM cmb1
	AFTER FIELD cmb1 
	NEXT FIELD cmb1
	ON ACTION act_bt1
		DISPLAY "https://querix.com/products/lycia/downloads/" TO br1
	ON ACTION act_bt2
		DISPLAY "https://querix.com/products/lycia/release-notes/" TO br1
	ON ACTION act_cmb
		LET n = cmb.GetItemCount()
		FOR i = 1 TO n
			LET cmb_opt = cmb.GetItemText(i)
				CASE cmb_opt
					WHEN "FAQ"
						DISPLAY "https://querix.atlassian.net/wiki/spaces/QKB/overview" TO br1
					WHEN "Lycia for beginners"
						DISPLAY "https://querix.com/go/beginner/index.htm" TO br1
					WHEN "Online documentation (full version)"
						DISPLAY "https://querix.com/go/lycia/index.htm" TO br1
				END CASE
		END FOR
	ON ACTION cancel
    EXIT INPUT 
END INPUT

END MAIN