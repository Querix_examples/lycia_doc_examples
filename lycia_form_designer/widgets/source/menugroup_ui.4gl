##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE menu ui.MenuBar
DEFINE menugroup ui.MenuGroup
DEFINE menubaritems DYNAMIC ARRAY OF ui.MenuItem

CALL ui.Interface.setType("container")
CALL ui.Application.GetCurrent().setMenuType("Menu")
CALL ui.Interface.LoadStartMenu("menugroup_ui")
LET menu = ui.MenuBar.Forname("mb1")

LET menugroup = ui.MenuGroup.create("MyMenuGroup")
CALL menugroup.setText("Menu Group")

CALL menugroup.setMenuItems([create_menu_command("id1", "exit", "act1")])

LET menubaritems = menu.getMenuItems()
CALL menubaritems.insert(1, menugroup)
CALL menu.setMenuItems(menubaritems)

MENU
	ON ACTION "act1"
	  EXIT MENU
END MENU

END MAIN

 FUNCTION create_menu_command(id, txt, eventName)
   DEFINE id, txt, eventName STRING
   DEFINE act ui.BackgroundServerEventHandler
   DEFINE menubaritems DYNAMIC ARRAY OF ui.MenuItem
   DEFINE menucommand ui.MenuCommand

   LET act = ui.BackgroundServerEventHandler.Create()
   CALL act.SetCallBackAction(eventName)    

   LET menucommand = ui.MenuCommand.create(id)
   CALL menucommand.setText(txt)
   CALL menucommand.SetOninvoke(act)
   
   RETURN menucommand
 END FUNCTION