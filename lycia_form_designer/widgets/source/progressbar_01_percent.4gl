##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE i SMALLINT
DEFINE pb_ui ui.ProgressBar
	
OPEN WINDOW w WITH FORM "progressbar_01_percent" ATTRIBUTE(BORDER)
LET pb_ui = ui.ProgressBar.Forname("pb1")

FOR i = 1 TO 5
  DISPLAY i TO pb1
  CALL fgl_getkey()
END FOR

END MAIN