##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE rg ui.RadioGroup
DEFINE r1, r2, r3 ui.Radio

OPEN WINDOW w WITH FORM "radio_ui" ATTRIBUTE(BORDER)
LET rg = ui.RadioGroup.forName("f1")
CALL rg.setEnable(TRUE)
LET r1 = ui.Radio.forName("f1_1")
LET r2 = ui.Radio.forName("f1_2")
LET r3 = ui.Radio.forName("f1_3")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("change", "change" , "qx://application/create.svg")
	ON ACTION "change"
    CALL r1.SetTitle("ONE")
    CALL r2.SetTitle("TWO")
    CALL r3.SetTitle("THREE")
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN