##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE grid ui.GridPanel
DEFINE cmb ui.ComboBox
DEFINE rLocation ui.GridItemLocation
DEFINE option_text, option_value, remove_option  STRING
DEFINE i, n INTEGER

CALL ui.Interface.setType("container")
CALL ui.Interface.loadStartMenu("combobox_ui_01_menu.sm2")
CALL ui.Application.GetCurrent().setMenuType("Tree")

OPEN WINDOW w WITH FORM "combobox_ui_01" ATTRIBUTE(BORDER)
--OPEN WINDOW w WITH FORM "combobox_ui_01" ATTRIBUTE(BORDER, STYLE="full-screen")

LET grid = ui.GridPanel.ForName("rootContainer")

MENU
  ON ACTION "act_create_cmb"
    LET cmb = ui.Combobox.Create("cmb1", "rootContainer")
    LET rLocation.GridHeight = 1
    LET rLocation.GridWidth = 1
    LET rLocation.GridX = 0
    LET rLocation.GridY = 0
    CALL cmb.SetGridItemLocation(rLocation)
    --CALL cmb.SetPreferredSize(["150px", "100px"])
  ON ACTION "act_props"
    CALL cmb.SetText("Choose one option")
    CALL cmb.SetEnable(TRUE)
  ON ACTION "act_add" ATTRIBUTES(ACCELERATOR="control-m")
    LET option_text = fgl_winprompt(5, 3, "Enter the text for your new combo box option.", "", 25, 0)
    LET option_value = fgl_winprompt(5, 3, "Enter the value of your new combo box option.", "", 25, 0)
    IF option_value IS NOT NULL THEN 
      CALL cmb.AddItem(option_value, option_text)
      ELSE
        LET option_value = fgl_winprompt(5, 3, "You must enter the value of your new combo box option.", "", 25, 0)
    END IF
  ON ACTION "act_view"
    DISPLAY "The whole number of options: ", trim(cmb.GetItemCount())
    LET n = cmb.GetItemCount()
    FOR i = 1 TO n
      --DISPLAY i, " - ", cmb.GetItemText(i), ", ", cmb.GetItemName(i), "."
      DISPLAY "index: ", trim(i)
      DISPLAY "text: ", cmb.GetItemText(i)
      DISPLAY "value: ", cmb.GetItemName(i)
      DISPLAY ""
    END FOR
  ON ACTION "act_css"
    CALL ui.Interface.frontcall("html5", "styleImport", ["qx://application/beautify.css", "copy"], [])
  ON ACTION "act_remove"
    LET remove_option = fgl_winprompt(5, 3, "Type the option's value to remove the option.", "", 25, 0)
    IF remove_option IS NOT NULL THEN
      CALL cmb.RemoveItem(remove_option)
    END IF
  ON ACTION "act_clear"
    CALL cmb.Clear()
  ON ACTION "act_docs"
    OPEN WINDOW w1 WITH FORM "combobox_ui_01_open_docs" ATTRIBUTE(BORDER, STYLE="full-screen")
    DISPLAY "https://querix.com/go/lycia/index.htm#06_reference/ui/forms/widgets/combobox.htm" to br1
  ON ACTION "act_exit"
    EXIT PROGRAM
END MENU

END MAIN