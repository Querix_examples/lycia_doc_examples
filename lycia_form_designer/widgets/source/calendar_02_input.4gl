##########################################################################
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE calendar_var DATE

OPEN WINDOW w WITH FORM "calendar_02_input" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("input" ,"input" , "qx://application/calendar_today.svg")
    CALL fgl_setactionlabel("display" ,"display" , "qx://application/search.svg")
  COMMAND "input"
    INPUT calendar_var FROM calendar_widget_1
  COMMAND "display"
    DISPLAY calendar_var TO calendar_widget_2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN
