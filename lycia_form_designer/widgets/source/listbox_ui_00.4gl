##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE grid ui.GridPanel
DEFINE lbx ui.ListBox
DEFINE rLocation ui.GridItemLocation
DEFINE option_text, option_value, remove_option  STRING
DEFINE i, n INTEGER

CALL ui.Interface.setType("container")
CALL ui.Interface.loadStartMenu("listbox_ui_00_menu.sm2")
--CALL ui.Application.GetCurrent().setMenuType("Tree")

OPEN WINDOW w WITH FORM "listbox_ui_00" ATTRIBUTE(BORDER)
--OPEN WINDOW w WITH FORM "listbox_ui_00" ATTRIBUTE(BORDER, STYLE="full-screen")

LET grid = ui.GridPanel.ForName("rootContainer")

MENU
  ON ACTION "act_create"
    LET lbx = ui.ListBox.Create("lbx1", "rootContainer")
    LET rLocation.GridHeight = 1
    LET rLocation.GridWidth = 1
    LET rLocation.GridX = 0
    LET rLocation.GridY = 0
    CALL lbx.SetGridItemLocation(rLocation)
    CALL lbx.SetPreferredSize(["150px", "100px"])
  ON ACTION "act_add" ATTRIBUTES(ACCELERATOR="control-m")
    LET option_text = fgl_winprompt(5, 3, "Enter the text for your new list box option.", "", 25, 0)
    LET option_value = fgl_winprompt(5, 3, "Enter the value of your new list box option.", "", 25, 0)
    IF option_value IS NOT NULL THEN 
      CALL lbx.AddItem(option_value, option_text)
      ELSE
        LET option_value = fgl_winprompt(5, 3, "You must enter the value of your new list box option.", "", 25, 0)
    END IF
  ON ACTION "act_view"
    DISPLAY "The whole number of options: ", trim(lbx.GetItemCount())
    LET n = lbx.GetItemCount()
    FOR i = 1 TO n
      --DISPLAY i, " - ", lbx.GetItemText(i), ", ", lbx.GetItemName(i), "."
      DISPLAY "index: ", trim(i)
      DISPLAY "text: ", lbx.GetItemText(i)
      DISPLAY "value: ", lbx.GetItemName(i)
      DISPLAY ""
    END FOR  
  ON ACTION "act_props"
    -- CALL lbx.SetVerticalAlignment("Stretch")
    CALL lbx.SetEnableMultiselection(TRUE)
    CALL lbx.SetSelectedItems(1)
  {ON ACTION "act_css"
    CALL ui.Interface.frontcall("html5", "styleImport", ["qx://application/beautify.css", "copy"], [])}
  ON ACTION "act_remove"
    LET remove_option = fgl_winprompt(5, 3, "Type the option's value to remove the option.", "", 25, 0)
    IF remove_option IS NOT NULL THEN
      CALL lbx.RemoveItem(remove_option)
    END IF
  ON ACTION "act_clear"
    CALL lbx.Clear()
  ON ACTION "act_docs"
    OPEN WINDOW w1 WITH FORM "listbox_ui_00_open_docs" ATTRIBUTE(BORDER, STYLE="full-screen")
    DISPLAY "https://querix.com/go/lycia/index.htm#06_reference/ui/forms/widgets/listbox.htm" to br1
  ON ACTION "act_exit"
    EXIT PROGRAM
END MENU

END MAIN