##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1 STRING
DEFINE dtls STRING

OPEN WINDOW w WITH FORM "label_md" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("details", "details" , "qx://application/search.svg")
  ON ACTION "details"
    LET dtls = "Most commonly used form properties:\n",
           "allowNewLines, text, image, classnames\n",
           "\n",
           "ui element:\n",
           "ui.Label\n",
           "\n",
           "Associated ui methods:\n",
           "DISPLAY TO"
    CALL fgl_winmessage("", dtls, "")
  ON ACTION  "exit"
    EXIT MENU
END MENU


END MAIN