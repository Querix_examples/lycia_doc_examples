##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE cmb ui.ComboBox

OPEN WINDOW w WITH FORM "combobox_ui_02_add_remove_clear" ATTRIBUTE(BORDER)
LET cmb = ui.ComboBox.forName("f1")

MENU
	BEFORE MENU
		CALL fgl_setactionlabel("count" ,"count" , "qx://application/search.svg")
		CALL fgl_setactionlabel("add" ,"add" , "qx://application/add.svg")
		CALL fgl_setactionlabel("remove" ,"remove" , "qx://application/remove.svg")
		CALL fgl_setactionlabel("clear" ,"clear" , "qx://application/delete.svg")
		CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "count"
    DISPLAY cmb.GetItemCount()
  ON ACTION "add"
    CALL cmb.AddItem("added", "new")
  ON ACTION "remove"
    CALL cmb.RemoveItem("pre")
  ON ACTION "clear"
    CALL cmb.Clear()
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN