##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE r1, r2, r3 STRING
DEFINE output STRING

OPEN WINDOW w1 WITH FORM "combobox_06_field_display" ATTRIBUTE(BORDER)
INPUT r1, r2, r3 from f1, f2, f3

LET output = "#1: ", r1,"\n",
  				"#2: ", r2,"\n",
  				"#3: ", r3,"\n"
DISPLAY BY NAME output 

MENU
    ON ACTION "Exit"
      EXIT MENU
  END MENU    
END MAIN