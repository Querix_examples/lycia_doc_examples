##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#Two different placeholders have the same id
MAIN

OPEN WINDOW w1 WITH FORM "ph_03_one_phid_01" ATTRIBUTE(BORDER)
OPEN WINDOW w2 WITH FORM "ph_03_one_phid_02" ATTRIBUTE(BORDER)

MENU
	COMMAND "add"
		OPEN WINDOW w3 INTO "ph" WITH FORM "ph_03_one_phid_01_into"
		OPEN WINDOW w4 INTO "ph" WITH FORM "ph_03_one_phid_01_into"
	COMMAND "exit"
		EXIT MENU
END MENU
END MAIN