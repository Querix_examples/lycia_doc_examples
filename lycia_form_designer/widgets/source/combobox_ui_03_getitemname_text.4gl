##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE cmb ui.Combobox
  DEFINE input_var STRING
  DEFINE i, n INTEGER
    
OPEN WINDOW w WITH FORM "combobox_ui_03_getitemname_text" ATTRIBUTE(BORDER)
LET cmb = ui.Combobox.ForName("f1")

MENU
	BEFORE MENU
		CALL fgl_setactionlabel("display all" ,"display all" , "qx://application/done_all.svg")
		CALL fgl_setactionlabel("display one" ,"display one" , "qx://application/done.svg")
		CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "display all"
    LET n = cmb.GetItemCount()
    FOR i = 1 TO n
      DISPLAY "The option's text is ", cmb.GetItemText(i), "."
      DISPLAY "The option's value is ", cmb.GetItemName(i), "."
    END FOR
  ON ACTION "display one"
    MESSAGE "Both methods need the option's index."
    LET input_var = fgl_winprompt(5, 3, "Type the option's index (Example: 3)", "", 25, 0)
    IF input_var IS NOT NULL THEN
      DISPLAY "The option's text is ", cmb.GetItemText(input_var), "."
      DISPLAY "The option's value is ", cmb.GetItemName(input_var), "."
    END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN