##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE cmb ui.Combobox
  DEFINE value_var, index_var STRING
    
OPEN WINDOW w WITH FORM "combobox_ui_05_getitemtext_getindexof" ATTRIBUTE(BORDER)
LET cmb = ui.Combobox.ForName("f1")

MENU
	BEFORE MENU
		CALL fgl_setactionlabel("learn index" ,"learn index" , "qx://application/one.svg")
		CALL fgl_setactionlabel("learn text" ,"learn text" , "qx://application/font.svg")
		CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "learn index"
    LET value_var = fgl_winprompt(5, 3, "Type the value (Example: value2)", "", 25, 0)
    IF value_var IS NOT NULL THEN
      DISPLAY "The option's index is ", cmb.GetIndexOf(value_var), "."
    END IF
  ON ACTION "learn text"
    LET index_var = fgl_winprompt(5, 3, "Type the option's index (Example: 3)", "", 25, 0)
    IF index_var IS NOT NULL THEN
      DISPLAY "The option's text is ", cmb.getItemText(index_var), "."
    END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN