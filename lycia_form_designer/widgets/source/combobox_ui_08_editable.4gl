##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE cmb ui.ComboBox

OPEN WINDOW w WITH FORM "combobox_ui_08_editable" ATTRIBUTE(BORDER)
LET cmb = ui.ComboBox.forName("f1")

MENU
	BEFORE MENU
		CALL fgl_setactionlabel("make editable" ,"make editable" , "qx://application/speaker_notes.svg")
		CALL fgl_setactionlabel("make uneditable" ,"make uneditable" , "qx://application/speaker_notes_off.svg")
		CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "make editable"
    CALL cmb.SetEditable(TRUE)
  ON ACTION "make uneditable"
    CALL cmb.SetEditable(FALSE)
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN