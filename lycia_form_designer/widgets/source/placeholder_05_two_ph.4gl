##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#Two different targets are set by OPEN WINDOW ... INTO "placeholder_id" and in .qxtheme

MAIN

OPEN WINDOW w1 WITH FORM "ph_05_two_phs" ATTRIBUTE(BORDER)

MENU
	COMMAND "add"
		OPEN WINDOW w2 INTO "ph1" WITH FORM "ph_05_two_phs_into"
	COMMAND "exit"
		EXIT MENU
END MENU
END MAIN