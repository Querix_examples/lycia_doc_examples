##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE w1 ui.Window
DEFINE tb_ui, tb_both ui.ToolbarButton
DEFINE img ui.Image

CALL w1.openWithForm("w1", "tlb_bttn_ set_icon", 5, 5,"border")

LET tb_ui = ui.ToolBarButton.Forname("tbb1")
LET tb_both = ui.ToolBarButton.Forname("tbb3")

CALL img.SetImageUrl("qx://application/cloud_full.svg")

CALL tb_ui.setImage(img)
CALL tb_both.setImage(img)

CALL tb_ui.setText("ui")
CALL tb_both.setText("ui both")

MENU
	ON ACTION act
		EXIT MENU
END MENU

END MAIN