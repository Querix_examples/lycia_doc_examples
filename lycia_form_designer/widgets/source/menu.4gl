##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
OPEN WINDOW w WITH FORM "menu" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("act6", "act6" , "qx://application/create.svg")
  ON ACTION "act1"
    DISPLAY "This action was triggered by the menu command 'One'."
  ON ACTION "act2"
    DISPLAY "This action was triggered by the menu command 'Two'."
  ON ACTION "act3"
    DISPLAY "This action was triggered by the menu command 'Three'."
  ON ACTION "act4"
    EXIT MENU
  ON ACTION "act6"
    MESSAGE "Hello, world!"
  ON ACTION "act7"
    EXIT MENU

END MENU


END MAIN