MAIN
DEFINE tb_group ui.ToolbarGroup
DEFINE tb_button ui.ToolbarButton
DEFINE buttonEvent ui.BackgroundServerEventHandler

OPEN WINDOW win WITH FORM "toolbar_ui" ATTRIBUTE(BORDER)

CALL fgl_getkey()

LET tb_group  = ui.ToolbarGroup.Create("tbg","toolbar")
LET tb_button = ui.ToolbarButton.Create("bt1","tbg")
CALL tb_button.SetText("form button")
LET buttonEvent = ui.BackgroundServerEventHandler.Create()
CALL buttonEvent.SetCallBackAction("display")
CALL tb_button.SetOnInvoke(buttonEvent)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display", "display" , "qx://application/search.svg")
	ON ACTION "display"
		DISPLAY "This toolbar button was created and associated with an action via ui methods."
	ON ACTION "exit"
		EXIT MENU
END MENU

END MAIN