##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1 STRING
DEFINE cmb ui.ComboBox

OPEN WINDOW w WITH FORM "combobox_ui_06_autonext_tocase" ATTRIBUTE(BORDER)
LET cmb = ui.ComboBox.forName("f1")

CALL cmb.SetEditable(TRUE)
CALL cmb.setToCase("Up")
CALL cmb.setAutonext(1)

INPUT BY NAME f1 WITHOUT DEFAULTS

CALL fgl_getkey()
END MAIN