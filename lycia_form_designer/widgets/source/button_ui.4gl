##########################################################################
# Widgets Project                                                        #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE bt_ui1,
       bt_ui2     ui.Button,
       image_ui   ui.Image,
       isImageSet SMALLINT

OPEN WINDOW w1 WITH FORM 'button_ui' ATTRIBUTE(BORDER) 

LET bt_ui1 = ui.Button.forName("bt1")
CALL image_ui.setImageUrl('qx://application/visibility.svg')
CALL bt_ui1.setText("Button for image")
CALL bt_ui1.SetEnable(TRUE)

LET bt_ui2 = ui.Button.forName("bt2")
LET isImageSet = 0
CALL bt_ui2.SetIsToggleButton(TRUE)
CALL bt_ui2.setText("Toggle Button")
CALL bt_ui2.SetEnable(TRUE)

MENU
	BEFORE MENU
		CALL fgl_setactionlabel("image" ,"image" , "qx://application/palette.svg")
		CALL fgl_setactionlabel("toggle" ,"toggle" , "qx://application/gavel.svg")
		CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
	ON ACTION "image"
		IF isImageSet THEN 
			LET isImageSet = 0
				CALL bt_ui1.SetImage("")
			ELSE
				LET isImageSet = 1
				CALL bt_ui1.SetImage(image_ui)
			END IF
	ON ACTION "toggle"
		IF bt_ui2.GetIsToggleButton() THEN
			CALL bt_ui2.SetIsToggleButton(FALSE)
			CALL bt_ui2.setText("Button")
		ELSE
			CALL bt_ui2.SetIsToggleButton(TRUE)
			CALL bt_ui2.setText("Toggle Button")
		END IF
	ON ACTION exit
		EXIT MENU  
	END MENU
END MAIN
