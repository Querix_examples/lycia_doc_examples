##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#Opens two new windows to two different placeholders by OPEN WINDOW ... INTO "placeholder_id"

MAIN

OPEN WINDOW w1 WITH FORM "ph_01_open_into_phid_02_multi_01" ATTRIBUTE(BORDER)
OPEN WINDOW w2 WITH FORM "ph_01_open_into_phid_02_multi_02" ATTRIBUTE(BORDER)

MENU
	COMMAND "add"
		OPEN WINDOW w3 INTO "ph1" WITH FORM "ph_01_open_into_phid_02_multi_01_into"
		OPEN WINDOW w4 INTO "ph2" WITH FORM "ph_01_open_into_phid_02_multi_02_into"
	COMMAND "exit"
		EXIT MENU
END MENU
END MAIN