##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE cmb ui.ComboBox
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "combobox_ui_10_setrequired" ATTRIBUTE(BORDER)
LET cmb = ui.ComboBox.forName("f1")

CALL cmb.setRequired(TRUE)

INPUT BY NAME f1 WITHOUT DEFAULTS
INPUT BY NAME f2 WITHOUT DEFAULTS

CALL fgl_getkey()
END MAIN