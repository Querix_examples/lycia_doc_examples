##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1,f2 STRING
DEFINE cb ui.ComboBox

OPEN WINDOW w WITH FORM "combobox_03_autocomplete" ATTRIBUTE(BORDER)
INPUT BY NAME f1,f2
	AFTER INPUT
		DISPLAY f1 TO f1_lb
		DISPLAY f2 TO f2_lb
END INPUT
CALL fgl_getkey()
END MAIN