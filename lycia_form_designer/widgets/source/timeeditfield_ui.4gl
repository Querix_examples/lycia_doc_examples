##########################################################################
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#Usage: 
# - Run app

MAIN
 DEFINE tef ui.TimeEditField
 DEFINE grid ui.GridPanel
 DEFINE f1 STRING
 DEFINE rLocation ui.GridItemLocation

 
 OPEN WINDOW w1 WITH FORM "timeeditfield_ui" ATTRIBUTE(BORDER)
 
 #Initialize grid container
 LET grid = ui.GridPanel.ForName("rootContainer")
 CALL grid.SetPreferredSize(["300px","50px"])

 #define Calendar location in grid
 LET tef = ui.TimeEditField.create("f1","rootContainer")
 LET rLocation.GridHeight = 1
 LET rLocation.GridWidth = 1
 LET rLocation.GridX = 0
 LET rLocation.GridY = 0
 CALL tef.SetGridItemLocation(rLocation)
 CALL tef.SetHorizontalAlignment("Center")
 CALL tef.SetVerticalAlignment("Center")
 CALL tef.SetPreferredSize(["275px","24px"])
 CALL tef.SetClassNames("leading-button")
 
 INPUT BY NAME f1
END MAIN