##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#Opens one new window to the placeholder by OPEN WINDOW ... INTO "placeholder_id"

MAIN

OPEN WINDOW w1 WITH FORM "placeholder_01_open_into_phid_01_single" ATTRIBUTE(BORDER)

MENU
	COMMAND "add"
		OPEN WINDOW w2 INTO "ph1" WITH FORM "placeholder_01_open_into_phid_01_single_into"
	COMMAND "exit"
		EXIT MENU
END MENU
END MAIN