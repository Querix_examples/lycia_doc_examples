##########################################################################
# Created by Alexey Printsevsky, Eugenia Pyzina                          #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE menu ui.MenuBar
DEFINE menugroup1, menugroup2 ui.MenuGroup
DEFINE menubaritems DYNAMIC ARRAY OF ui.MenuItem

CALL ui.Interface.setType("container")
CALL ui.Application.GetCurrent().setMenuType("Tree")
CALL ui.Interface.LoadStartMenu("menu_ui")
LET menu = ui.MenuBar.Forname("mb1")

LET menugroup1 = ui.MenuGroup.create("MyMenuGroup01")
CALL menugroup1.setText("Menu Group 01")

CALL menugroup1.setMenuItems([create_menu_command("id1", "One", "act1"),
							create_menu_command("id2", "Two", "act2")])

LET menubaritems = menu.getMenuItems()
CALL menubaritems.insert(1, menugroup1)
CALL menu.setMenuItems(menubaritems)

LET menugroup2 = ui.MenuGroup.create("MyMenuGroup02")
CALL menugroup2.setText("Menu Group 02")

CALL menugroup2.setMenuItems([create_menu_command("id3", "Three", "act3"),
							create_menu_command("id4", "Four", "act4"),
							create_menu_command("id5", "Exit", "exit")])

LET menubaritems = menu.getMenuItems()
CALL menubaritems.insert(2, menugroup2)
CALL menu.setMenuItems(menubaritems)

MENU
	ON ACTION "act1"
		DISPLAY "This action was triggered by the menu command 'One'."
	ON ACTION "act2"
		DISPLAY "This action was triggered by the menu command 'Two'."
	ON ACTION "act3"
		DISPLAY "This action was triggered by the menu command 'Three'."
    ON ACTION "act4"
		DISPLAY "This action was triggered by the menu command 'Four'."
	ON ACTION "exit"
		EXIT MENU
END MENU
END MAIN

 FUNCTION create_menu_command(id, txt, eventName)
   DEFINE id, txt, eventName STRING
   DEFINE act ui.BackgroundServerEventHandler
   DEFINE menubaritems DYNAMIC ARRAY OF ui.MenuItem
   DEFINE menucommand ui.MenuCommand

   LET act = ui.BackgroundServerEventHandler.Create()
   CALL act.SetCallBackAction(eventName)    

   LET menucommand = ui.MenuCommand.create(id)
   CALL menucommand.setText(txt)
   CALL menucommand.SetOninvoke(act)
   
   RETURN menucommand
 END FUNCTION