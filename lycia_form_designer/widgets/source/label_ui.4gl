##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE grid ui.GridPanel
DEFINE lb ui.Label
DEFINE rLocation ui.GridItemLocation
DEFINE fnt ui.Font
DEFINE dtls STRING

CALL ui.Interface.setType("container")

OPEN WINDOW w WITH FORM "label_ui" ATTRIBUTE(BORDER)

LET grid = ui.GridPanel.ForName("rootContainer")

MENU
	BEFORE MENU
		CALL fgl_setactionlabel("create" ,"create" , "qx://application/create.svg")
		CALL fgl_setactionlabel("size" ,"size" , "qx://application/one.svg")
		CALL fgl_setactionlabel("font" ,"font" , "qx://application/font.svg")
		CALL fgl_setactionlabel("details" ,"details" , "qx://application/search.svg")
		CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "create"
    LET lb = ui.Label.Create("lb1", "rootContainer")
    LET rLocation.GridHeight = 1
    LET rLocation.GridWidth = 1
    LET rLocation.GridX = 0
    LET rLocation.GridY = 0
    CALL lb.SetGridItemLocation(rLocation)
    CALL lb.SetText("This is a label.")
  ON ACTION "size"
    CALL lb.setPreferredSize(["250px", "150px"])
    CALL lb.setHorizontalAlignment("Center")
    CALL lb.setVerticalAlignment("Center")
  ON ACTION "font"
    LET fnt.Family = ["Cambria"]
    LET fnt.Bold = TRUE
    LET fnt.Italic = FALSE
    LET fnt.Underline = FALSE
    LET fnt.FontSize = 25
    CALL lb.SetFont(fnt)
  ON ACTION "details"
    DISPLAY "GetText() = ", lb.GetText()
    DISPLAY "GetPreferredSize() = ", lb.GetPreferredSize()
    DISPLAY "GetImage() = ", lb.GetImage()
    DISPLAY "GetEnable = ", lb.GetEnable()
    DISPLAY "GetIsDynamic() = ", lb.GetIsDynamic()
    DISPLAY "GetOnInvoke() = ", lb.GetOnInvoke()
    DISPLAY "GetAllowNewlines() = ", lb.GetAllowNewlines()
    DISPLAY "GetFont() = ", lb.GetFont()     
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN