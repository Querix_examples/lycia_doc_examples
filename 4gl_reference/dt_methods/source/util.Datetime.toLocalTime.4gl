##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE local DATETIME YEAR TO SECOND
    DISPLAY "UTC: 2017-02-23 12:34:56"
    LET local = util.Datetime.toLocalTime(DATETIME(2017-02-23 12:34:56) YEAR TO SECOND)
    DISPLAY "Local: ", local
  CALL fgl_getkey()
END MAIN