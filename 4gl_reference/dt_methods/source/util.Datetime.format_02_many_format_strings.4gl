##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DISPLAY CURRENT
    DISPLAY "%Y-%m-%d %H:%M", ":  ", util.Datetime.format(CURRENT, "%Y-%m-%d %H:%M")
    DISPLAY "%Y/%m/%d %H:%M:%S:%F", ":  ", util.Datetime.format(CURRENT, "%Y/%m/%d %H:%M:%S:%F")
    DISPLAY "%d-%m-%y %H:%M", ":  ", util.Datetime.format(CURRENT, "%d-%m-%y %H:%M")
    DISPLAY "%d-%B-%Y %H:%M:%S", ":  ", util.Datetime.format(CURRENT, "%d-%B-%Y %H:%M:%S")
    DISPLAY "%Y-%b-%d (%a) %I:%M", ":  ", util.Datetime.format(CURRENT, "%Y-%b-%d (%a) %I:%M")
    DISPLAY "%A, %B %d", ":  ", util.Datetime.format(CURRENT, "%A, %B %d")
    DISPLAY "%Y-%m-%d %p", ":  ", util.Datetime.format(CURRENT, "%Y-%m-%d %p")
    DISPLAY "%T", ":  ", util.Datetime.format(CURRENT, "%T")
    DISPLAY "[%d %h %Y %H:%M:%S]", ":  ", util.Datetime.format(CURRENT, "[%d %h %Y %H:%M:%S]")
    DISPLAY "%B %d, %Y", ":  ", util.Datetime.format(CURRENT, "%B %d, %Y")
    DISPLAY "%w %t = %t %A", ":  ", util.Datetime.format(CURRENT, "%w %t = %t %A")
    DISPLAY "%D %n %H:%M", ":  ", util.Datetime.format(CURRENT, "%D %n %H:%M")
  CALL fgl_getkey()
END MAIN