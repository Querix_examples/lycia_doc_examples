##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
    DISPLAY util.Strings.base64DecodeToString("QWFCYkNj")
    DISPLAY util.Strings.base64DecodeToString("YWJj")
    DISPLAY util.Strings.base64DecodeToString("QUJD")
    DISPLAY util.Strings.base64DecodeToString("MTIzNDU") 
CALL fgl_getkey()
END MAIN