##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
    DEFINE base64 STRING
    LET base64 = util.Strings.base64EncodeFromString("Müller")
    DISPLAY "for Müller"
    DISPLAY base64
    DISPLAY util.Strings.base64DecodeToString(base64)
    LET base64 = util.Strings.base64EncodeFromString("Muller")
    DISPLAY "for Muller"
    DISPLAY base64
    DISPLAY util.Strings.base64DecodeToString(base64)
    
    LET base64 = util.Strings.base64EncodeFromString("Bédard")
    DISPLAY "for Bédard"
    DISPLAY base64
    DISPLAY util.Strings.base64DecodeToString(base64)
    DISPLAY ""
    LET base64 = util.Strings.base64EncodeFromString("Bedard")
    DISPLAY "for Bedard"
    DISPLAY base64
    DISPLAY util.Strings.base64DecodeToString(base64)
CALL fgl_getkey()
END MAIN