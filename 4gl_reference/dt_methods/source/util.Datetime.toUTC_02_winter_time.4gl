##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
    DISPLAY "     Original UTC         Local time       toUTC(local-time)     (toUTC() - Original UTC)"
    CALL test_to_utc("2016-10-30 02:35:35")
    CALL test_to_utc("2016-10-30 03:35:35")
    CALL test_to_utc("2016-10-30 04:35:35")
    CALL test_to_utc("2016-10-30 05:35:35")
  CALL fgl_getkey()
END MAIN

FUNCTION test_to_utc(utc)
    DEFINE utc, local, utc_upd DATETIME YEAR TO SECOND
    LET local = util.Datetime.toLocalTime(utc)
    LET utc_upd = util.Datetime.toUTC(local)
    DISPLAY SFMT("%1  %2  %3  %4", utc, local, utc_upd, utc_upd-utc)
END FUNCTION