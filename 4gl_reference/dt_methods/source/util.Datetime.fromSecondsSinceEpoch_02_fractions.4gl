##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE dt1 DATETIME YEAR TO SECOND,
         dt2 DATETIME YEAR TO FRACTION(3)
    LET dt1 = util.Datetime.fromSecondsSinceEpoch(315630296)
    LET dt2 = util.Datetime.fromSecondsSinceEpoch(315630296.789)
    DISPLAY "Unix epoch time #1:  315630296"   
    DISPLAY "Human readable time #1:  ", dt1
    DISPLAY "Unix epoch time #2:  315630296.789"   
    DISPLAY "Human readable time #2:  ", dt2
  CALL fgl_getkey()
END MAIN