##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
    DISPLAY "2017-02-23 17:15:56.12"
    DISPLAY "Unix epoch time for DATETIME YEAR TO SECOND:  " 
    DISPLAY util.Datetime.toSecondsSinceEpoch(DATETIME(2017-02-24 17:15:56) YEAR TO SECOND)
    DISPLAY "Unix epoch time for DATETIME YEAR TO FRACTION(2):  " 
    DISPLAY util.Datetime.toSecondsSinceEpoch(DATETIME(2017-02-24 17:15:56.12) YEAR TO FRACTION(2))
  CALL fgl_getkey()
END MAIN