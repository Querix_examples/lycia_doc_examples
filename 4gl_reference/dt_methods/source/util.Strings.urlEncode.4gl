##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DISPLAY "ABCDE"
DISPLAY util.Strings.urlEncode("ABCDE")
DISPLAY "abcde"
DISPLAY util.Strings.urlEncode("abcde")
DISPLAY "0123456789-_.~"
DISPLAY util.Strings.urlEncode("0123456789-_.~")
DISPLAY "£¥$€"
DISPLAY util.Strings.urlEncode("£¥$€")
DISPLAY "àèçÜÖÄ"
DISPLAY util.Strings.urlEncode("àèçÜÖÄ")
DISPLAY "АБВГД"
DISPLAY util.Strings.urlEncode("АБВГД")
DISPLAY "@%<=>"
DISPLAY util.Strings.urlEncode("@%<=>")
DISPLAY "Hello, world!"
DISPLAY util.Strings.urlEncode("Hello, world!")
CALL fgl_getkey()
END MAIN