##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE dt_ym DATETIME YEAR TO MINUTE,
         dt_yd DATETIME YEAR TO DAY,
         dt_yy DATETIME YEAR TO YEAR
    LET dt_ym = util.Datetime.parse("2017-02-22 14:15", "%Y-%m-%d %H:%M")
      DISPLAY dt_ym
    LET dt_yd = util.Datetime.parse("2017-02-22 14:15", "%Y-%m-%d")
      DISPLAY dt_yd
    LET dt_yy = util.Datetime.parse("2017-02-22 14:15", "%Y")
      DISPLAY dt_yy
  CALL fgl_getkey()
END MAIN