##########################################################################
# Data types methods Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DISPLAY "Hello%2c+world!"
DISPLAY util.Strings.urlDecode("Hello%2c+world!")
DISPLAY "ABCDE"
DISPLAY util.Strings.urlDecode("ABCDE")
DISPLAY "%c3%a0%c3%a8%c3%a7%c3%9c%c3%96%c3%84"
DISPLAY util.Strings.urlDecode("%c3%a0%c3%a8%c3%a7%c3%9c%c3%96%c3%84")
DISPLAY "%3c%3d%3e"
DISPLAY util.Strings.urlDecode("%3c%3d%3e")
DISPLAY "%3C%3D%3E"
DISPLAY util.Strings.urlDecode("%3C%3D%3E")
DISPLAY "%3c%%3e"
DISPLAY util.Strings.urlDecode("%3c%%3e")
CALL fgl_getkey()
END MAIN