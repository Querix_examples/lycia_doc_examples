##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE cmb ui.ComboBox
DEFINE i, n INTEGER
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "ui.ComboBox.RemoveItem_02_for" ATTRIBUTE(BORDER, STYLE="Panels1")
LET cmb = ui.ComboBox.forName("f1")
MESSAGE "Here values of ComboBox options are IntegerLiteral."

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("count" ,"count" , "qx://application/search.svg")
    CALL fgl_setactionlabel("remove i = 1 to n" ,"remove i = 1 to n" , "qx://application/backward.svg")
    CALL fgl_setactionlabel("remove i = n to 1" ,"remove i = n to 1" , "qx://application/forward.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "count"
    LET tmp = "The whole number of options: ", trim(cmb.GetItemCount()), "."
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "remove i = 1 to n"
    LET n = cmb.GetItemCount()
    FOR i = 1 TO n  
      LET tmp = "You remove this option:\n",
                "text: ", cmb.GetItemText(i), "\n",
                "value: ", cmb.GetItemName(i), "\n",
                "\n",
                "Options left: ", trim(cmb.GetItemCount() - 1)          
       CALL cmb.RemoveItem(i)
       CALL fgl_winmessage("", tmp, "")
    END FOR
  ON ACTION "remove i = n to 1"
    LET n = cmb.GetItemCount()
    FOR i = n TO 1 STEP -1
      LET tmp = "You remove this option:\n",
                "text: ", cmb.GetItemText(i), "\n",
                "value: ", cmb.GetItemName(i), "\n",
                "\n",
                "Options left: ", trim(cmb.GetItemCount() - 1)
      CALL cmb.RemoveItem(i)
      CALL fgl_winmessage("", tmp, "")
    END FOR
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN