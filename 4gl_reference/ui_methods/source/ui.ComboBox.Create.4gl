##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE grid ui.GridPanel
DEFINE cmb1, cmb2 ui.ComboBox
DEFINE rLocation ui.GridItemLocation
DEFINE option_text, option_value STRING

OPEN WINDOW w WITH FORM "ui.ComboBox.Create" ATTRIBUTE(BORDER)

LET grid = ui.GridPanel.ForName("rootContainer")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("create cmb1" ,"create cmb1" , "qx://application/one.svg")
    CALL fgl_setactionlabel("create cmb2" ,"create cmb2" , "qx://application/two.svg")
    CALL fgl_setactionlabel("populate" ,"populate" , "qx://application/add_group.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "create cmb1"
    LET cmb1 = ui.Combobox.Create("cmb1", "rootContainer")
    LET rLocation.GridHeight = 1
    LET rLocation.GridWidth = 1
    LET rLocation.GridX = 0
    LET rLocation.GridY = 0
    CALL cmb1.SetGridItemLocation(rLocation)
  ON ACTION "create cmb2"
    LET cmb2 = ui.Combobox.Create("cmb2", "rootContainer")
    LET rLocation.GridHeight = 1
    LET rLocation.GridWidth = 1
    LET rLocation.GridX = 0
    LET rLocation.GridY = 1
    CALL cmb2.SetGridItemLocation(rLocation)
  ON ACTION "populate"
    IF UPSHIFT(fgl_winquestion("", "What combo box do you want to populate?", "cmb1", "cmb1|cmb2", "question", 1))="CMB1" THEN
      LET option_text = fgl_winprompt(5, 3, "Enter the text for your new combo box option.", "", 25, 0)
      LET option_value = fgl_winprompt(5, 3, "Enter the value of your new combo box option.", "", 25, 0)
      CALL cmb1.AddItem(option_value, option_text)
  	ELSE 
      LET option_text = fgl_winprompt(5, 3, "Enter the text for your new combo box option.", "", 25, 0)
      LET option_value = fgl_winprompt(5, 3, "Enter the value of your new combo box option.", "", 25, 0)
      CALL cmb2.AddItem(option_value, option_text)
  	END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN