##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE menuVar TEXT
CALL ui.Interface.setType("container")
CALL ui.Interface.setName("Large datatypes")

LOCATE menuVar in file "ui.interface.loadstartmenu_01_large_dts.fm2"
CALL ui.Interface.loadStartMenu(menuVar)
CALL ui.Application.GetCurrent().setMenuType("Tree")

MENU "m"
  ON ACTION CANCEL
	EXIT MENU
  ON ACTION expanded
    DISPLAY "The menu group was expanded."
  ON ACTION act_load
    OPEN WINDOW w_load WITH FORM "ui.int.lsm_info_load" ATTRIBUTE(BORDER)
  ON ACTION act_byte
    OPEN WINDOW w_byte WITH FORM "ui.int.lsm_info_byte" ATTRIBUTE(BORDER)
  ON ACTION act_text
    OPEN WINDOW w_text WITH FORM "ui.int.lsm_info_text" ATTRIBUTE(BORDER)
END MENU
CALL fgl_getkey()
END MAIN