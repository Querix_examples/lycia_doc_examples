##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE cmb ui.Combobox
  DEFINE input_var, tmp STRING
  DEFINE i, n INTEGER
    
OPEN WINDOW w WITH FORM "ui.ComboBox.GetItemName" ATTRIBUTE(BORDER)
LET cmb = ui.Combobox.ForName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display all" ,"display all" , "qx://application/bookmarks.svg")
    CALL fgl_setactionlabel("display one" ,"display one" , "qx://application/bookmark.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "display all"
    LET n = cmb.GetItemCount()
    FOR i = 1 TO n
      LET tmp = tmp, "The option's value is ", cmb.GetItemName(i), ".\n"
    END FOR
    CALL fgl_winmessage("", tmp, "")
  ON ACTION "display one"
    LET input_var = fgl_winprompt(5, 3, "Enter the option's index (Example: 3).", "", 25, 0)
    IF input_var IS NOT NULL THEN
      LET tmp = "You look for the option which index is ", input_var, ".\n",
                "The option's value is ", cmb.GetItemName(input_var), "."
      CALL fgl_winmessage("", tmp, "")
    END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN