##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE cmb ui.ComboBox
DEFINE i, n INTEGER
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "ui.ComboBox.RemoveItem_01" ATTRIBUTE(BORDER)
LET cmb = ui.ComboBox.forName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display options" ,"display options" , "qx://application/search.svg")
    CALL fgl_setactionlabel("remove string" ,"remove string" , "qx://application/font_download.svg")
    CALL fgl_setactionlabel("remove integer" ,"remove integer" , "qx://application/one.svg")
    CALL fgl_setactionlabel("remove boolean" ,"remove boolean" , "qx://application/exposure.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "display options"
    LET n = cmb.GetItemCount()
    FOR i = 1 to n
      LET tmp = "The whole number of options: ", trim(cmb.GetItemCount()), "\n",
                "index: ", trim(i), "\n", 
                "text: ", cmb.GetItemText(i), "\n",
                "value: ", cmb.GetItemName(i), "\n"
      CALL fgl_winmessage("", tmp, "")
    END FOR
  ON ACTION "remove string"
    CALL cmb.RemoveItem("one")
  ON ACTION "remove integer"
    CALL cmb.RemoveItem(2)
  ON ACTION "remove boolean"
    CALL cmb.RemoveItem(TRUE)
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN