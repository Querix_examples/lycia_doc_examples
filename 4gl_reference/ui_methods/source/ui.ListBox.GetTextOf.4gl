##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE lbx ui.ListBox
  DEFINE input_var, tmp STRING
  DEFINE i, n INTEGER
    
OPEN WINDOW w WITH FORM "ui.ListBox.GetTextOf" ATTRIBUTE(BORDER)
LET lbx = ui.ListBox.ForName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("GetItemName" ,"GetItemName" , "qx://application/bookmarks.svg")
    CALL fgl_setactionlabel("getTextOf" ,"getTextOf" , "qx://application/bookmark.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "GetItemName"
    LET n = lbx.GetItemCount()
    FOR i = 1 TO n
      LET tmp = tmp, "The value for the option ", trim(i), " is ", lbx.GetItemName(i), ".\n"
    END FOR
    CALL fgl_winmessage("", tmp, "")  
  ON ACTION "getTextOf"
    LET input_var = fgl_winprompt(5, 3, "Type the option's value (Example: value3).", "", 25, 0)
    IF input_var IS NOT NULL THEN
      LET tmp = "For ", input_var, ", getTextOf() returns ", lbx.GetTextOf(input_var), "."
      CALL fgl_winmessage("", tmp, "")
    END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN