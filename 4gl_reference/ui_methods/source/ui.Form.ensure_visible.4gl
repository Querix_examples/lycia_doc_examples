##########################################################################
# Created by Eugenia Chubar   			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE f1 STRING
      
OPEN WINDOW w WITH FORM "ui.Form.ensure_visible" ATTRIBUTE(BORDER)
LET w = ui.Window.getCurrent()
LET f = w.getForm()

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("field" ,"field" , "qx://application/create.svg")
    CALL fgl_setactionlabel("element" ,"element" , "qx://application/image.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION field
    CALL f.ensureFieldVisible("f1")
  ON ACTION element
    CALL f.ensureElementVisible("lb2")
  ON ACTION "exit"
    EXIT MENU
END MENU
END MAIN