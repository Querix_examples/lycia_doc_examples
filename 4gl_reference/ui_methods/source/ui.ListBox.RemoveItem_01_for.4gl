##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE lbx ui.ListBox
  DEFINE i, n INTEGER
  DEFINE tmp STRING

OPEN WINDOW w WITH FORM "ui.ListBox.RemoveItem_01_for" ATTRIBUTE(BORDER, STYLE="Panels1")
LET lbx = ui.ListBox.forName("f1")
MESSAGE "Here values of ListBox options are IntegerLiteral."

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("count" ,"count" , "qx://application/search.svg")
    CALL fgl_setactionlabel("remove i = 1 to n" ,"remove i = 1 to n" , "qx://application/backward.svg")
    CALL fgl_setactionlabel("remove i = n to 1" ,"remove i = n to 1" , "qx://application/forward.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "count"
    CALL fgl_winmessage("", lbx.GetItemCount(), "") 
  ON ACTION "remove i = 1 to n"
    LET n = lbx.GetItemCount()
    FOR i = 1 TO n  
      LET tmp = "You remove this option:\n",
                "text: ", lbx.GetItemText(i), "\n",
                "value: ", lbx.GetItemName(i), "\n",
                "\n",
                "Options left: ", trim(lbx.GetItemCount() - 1)          
       CALL lbx.RemoveItem(i)
       CALL fgl_winmessage("", tmp, "")
    END FOR
  ON ACTION "remove i = n to 1"
    LET n = lbx.GetItemCount()
    FOR i = n TO 1 STEP -1
      LET tmp = "You remove this option:\n",
                "text: ", lbx.GetItemText(i), "\n",
                "value: ", lbx.GetItemName(i), "\n",
                "\n",
                "Options left: ", trim(lbx.GetItemCount() - 1)
      CALL lbx.RemoveItem(i)
      CALL fgl_winmessage("", tmp, "")
    END FOR
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN