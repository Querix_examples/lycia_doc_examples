##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE cmb ui.Combobox
DEFINE value_var, tmp STRING
    
OPEN WINDOW w WITH FORM "ui.ComboBox.GetIndexOf" ATTRIBUTE(BORDER)
LET cmb = ui.Combobox.ForName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("learn index" ,"learn index" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "learn index"
    LET value_var = fgl_winprompt(5, 3, "Enter the option's value (Example: value2)", "", 25, 0)
    IF value_var IS NOT NULL THEN
      LET tmp = "You have entered this value - ", value_var, ".\n",
                "The index of the option with this value is ", trim(cmb.GetIndexOf(value_var)), "."
      CALL fgl_winmessage("", tmp, "")
    END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN