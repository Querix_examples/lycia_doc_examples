##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE lbx ui.ListBox
DEFINE option_text, option_value STRING
DEFINE i, n INTEGER
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "ui.ListBox.AddItem_02" ATTRIBUTE(BORDER)
LET lbx = ui.ListBox.forName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("add hardcoded" ,"add hardcoded" , "qx://application/add_person.svg")
    CALL fgl_setactionlabel("add custom" ,"add custom" , "qx://application/add_group.svg")
    CALL fgl_setactionlabel("view" ,"view" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "add hardcoded"
    CALL lbx.AddItem("value", "text")
  ON ACTION "add custom"
    LET option_text = fgl_winprompt(5, 3, "Enter the text for your new list box option.", "", 25, 0)
    LET option_value = fgl_winprompt(5, 3, "Enter the value of your new list box option.", "", 25, 0)
    IF option_value IS NOT NULL THEN 
      CALL lbx.AddItem(option_value, option_text)
      ELSE
        LET option_value = fgl_winprompt(5, 3, "You must enter the value of your new list box option.", "", 25, 0)
    END IF
  ON ACTION "view" 
    LET n = lbx.GetItemCount()
    FOR i = 1 TO n
      LET tmp = "The whole number of options: ", trim(lbx.GetItemCount()), "\n",
                "index: ", trim(i), "\n", 
                "text: ", lbx.GetItemText(i), "\n",
                "value: ", lbx.GetItemName(i), "\n"               
      CALL fgl_winmessage("", tmp, "")
    END FOR 
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN