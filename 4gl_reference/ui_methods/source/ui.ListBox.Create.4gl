##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE grid ui.GridPanel
DEFINE lbx1, lbx2 ui.ListBox
DEFINE rLocation ui.GridItemLocation
DEFINE option_text, option_value STRING

OPEN WINDOW w WITH FORM "ui.ListBox.Create" ATTRIBUTE(BORDER)

LET grid = ui.GridPanel.ForName("rootContainer")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("create lbx1" ,"create lbx1" , "qx://application/one.svg")
    CALL fgl_setactionlabel("create lbx2" ,"create lbx2" , "qx://application/two.svg")
    CALL fgl_setactionlabel("populate" ,"populate" , "qx://application/add_group.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "create lbx1"
    LET lbx1 = ui.ListBox.Create("lbx1", "rootContainer")
    LET rLocation.GridHeight = 1
    LET rLocation.GridWidth = 1
    LET rLocation.GridX = 0
    LET rLocation.GridY = 0
    CALL lbx1.SetGridItemLocation(rLocation)
    CALL lbx1.SetMinSize(["","50px"])
  ON ACTION "create lbx2"
    LET lbx2 = ui.ListBox.Create("lbx2", "rootContainer")
    LET rLocation.GridHeight = 1
    LET rLocation.GridWidth = 1
    LET rLocation.GridX = 0
    LET rLocation.GridY = 1
    CALL lbx2.SetGridItemLocation(rLocation)
    CALL lbx2.SetMinSize(["","50px"])
  ON ACTION "populate"
    IF UPSHIFT(fgl_winquestion("", "What list box do you want to populate?", "lbx1", "lbx1|lbx2", "question", 1))="LBX1" THEN
      LET option_text = fgl_winprompt(5, 3, "Enter the text for your new list box option.", "", 25, 0)
      LET option_value = fgl_winprompt(5, 3, "Enter the value of your new list box option.", "", 25, 0)
      CALL lbx1.AddItem(option_value, option_text)
  	ELSE 
      LET option_text = fgl_winprompt(5, 3, "Enter the text for your new list box option.", "", 25, 0)
      LET option_value = fgl_winprompt(5, 3, "Enter the value of your new list box option.", "", 25, 0)
      CALL lbx2.AddItem(option_value, option_text)
  	END IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN