##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE lbx ui.ListBox
DEFINE i, n INTEGER
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "ui.ListBox.AddItem_01_types" ATTRIBUTE(BORDER)
LET lbx = ui.ListBox.forName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("add string" ,"add string" , "qx://application/font_download.svg")
    CALL fgl_setactionlabel("add integer" ,"add integer" , "qx://application/one.svg")
    CALL fgl_setactionlabel("add boolean" ,"add boolean" , "qx://application/exposure.svg")
    CALL fgl_setactionlabel("view" ,"view" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "add string"
    CALL lbx.AddItem("value", "string")
  ON ACTION "add integer"
    CALL lbx.AddItem(25, "integer")
  ON ACTION "add boolean"
    CALL lbx.AddItem(TRUE, "boolean")
  ON ACTION "view"
    LET n = lbx.GetItemCount()
    FOR i = 1 TO n
      LET tmp = "The whole number of options: ", trim(lbx.GetItemCount()), "\n",
                "index: ", trim(i), "\n", 
                "text: ", lbx.GetItemText(i), "\n",
                "value: ", lbx.GetItemName(i), "\n"               
      CALL fgl_winmessage("", tmp, "")
    END FOR
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN