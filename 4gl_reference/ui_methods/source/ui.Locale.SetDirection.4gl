MAIN
DEFINE f1 CHAR(20)
DEFINE tf ui.TextField
DEFINE lc ui.Locale

OPEN WINDOW w WITH FORM "ui.Locale.SetDirection" ATTRIBUTE(BORDER)
LET tf = ui.TextField.ForName("f1")
CALL tf.SetText("text")

INPUT BY NAME f1
  BEFORE INPUT
    CALL lc.SetDirection("RTL")
    CALL tf.SetLocale(lc)
  END INPUT
END MAIN