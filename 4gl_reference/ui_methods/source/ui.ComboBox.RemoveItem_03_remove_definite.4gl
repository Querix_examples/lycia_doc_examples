##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE remVal, f1, tmp STRING
DEFINE cmb ui.Combobox
DEFINE i, n INTEGER

OPEN WINDOW w WITH FORM "ui.ComboBox.RemoveItem_03_remove_definite" ATTRIBUTE (BORDER, STYLE="Panels1")
    LET cmb = ui.Combobox.ForName("f1")
  
MENU
  BEFORE MENU
    CALL fgl_setactionlabel("remove" ,"remove" , "qx://application/delete.svg")
    CALL fgl_setactionlabel("view" ,"view" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "remove"
    MESSAGE "Here values of ComboBox options are StringLiteral."
    LET remVal = fgl_winprompt(5, 3, "Type the option's value to remove it (Example: value3)", "", 25, 0)
    IF remVal IS NOT NULL THEN
      CALL cmb.RemoveItem(remVal)
    END IF
  ON ACTION "view"  
    LET n = cmb.GetItemCount()
    FOR i = 1 TO n
      LET tmp = "The whole number of options: ", trim(cmb.GetItemCount()), "\n",
                "index: ", trim(i), "\n", 
                "text: ", cmb.GetItemText(i), "\n",
                "value: ", cmb.GetItemName(i), "\n"               
      CALL fgl_winmessage("", tmp, "")
    END FOR 
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN