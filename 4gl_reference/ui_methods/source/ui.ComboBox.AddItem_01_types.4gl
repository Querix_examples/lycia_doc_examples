##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE cmb ui.ComboBox
DEFINE i, n INTEGER

OPEN WINDOW w WITH FORM "ui.ComboBox.AddItem_01_types" ATTRIBUTE(BORDER)
LET cmb = ui.ComboBox.forName("f1")

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("add string" ,"add string" , "qx://application/font_download.svg")
    CALL fgl_setactionlabel("add integer" ,"add integer" , "qx://application/one.svg")
    CALL fgl_setactionlabel("add boolean" ,"add boolean" , "qx://application/exposure.svg")
    CALL fgl_setactionlabel("view" ,"view" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "add string"
    CALL cmb.AddItem("value", "string")
  ON ACTION "add integer"
    CALL cmb.AddItem(25, "integer")
  ON ACTION "add boolean"
    CALL cmb.AddItem(TRUE, "boolean")
  ON ACTION "view"
    DISPLAY "The whole number of options: ", trim(cmb.GetItemCount())
    LET n = cmb.GetItemCount()
    FOR i = 1 TO n
      DISPLAY "index: ", trim(i)
      DISPLAY "text: ", cmb.GetItemText(i)
      DISPLAY "value: ", cmb.GetItemName(i)
      DISPLAY ""
    END FOR
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN