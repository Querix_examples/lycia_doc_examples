##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

CALL ui.Interface.loadActionDefaults("ads_02_menu")

OPEN WINDOW w WITH FORM "form_02_menu" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("act_1" ,"act_1" , "qx://application/one.svg")
    CALL fgl_setactionlabel("act_2" ,"act_2" , "qx://application/two.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg") 
	ON ACTION "act_1"
		DISPLAY "ACTION \"act_1\""
	ON ACTION "act_2"
		DISPLAY "ACTION \"act_2\""
	ON ACTION "exit"
		EXIT MENU
END MENU
END MAIN