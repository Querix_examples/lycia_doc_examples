##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

OPEN WINDOW w1 WITH FORM "multiple_open_window_01" ATTRIBUTE(BORDER)
OPEN WINDOW w2 WITH FORM "multiple_open_window_02" ATTRIBUTE(BORDER)
OPEN WINDOW w3 WITH FORM "multiple_open_window_03" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("display" ,"display" , "qx://application/create.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
	ON ACTION "display"
		CURRENT WINDOW IS w1
		DISPLAY "One" TO lb1
		CALL fgl_getkey()
		CURRENT WINDOW IS w2
		DISPLAY "Two" TO lb1
		CALL fgl_getkey()
		CURRENT WINDOW IS w3
		DISPLAY "Three" TO lb1
	ON ACTION "exit"
		EXIT MENU
END MENU

END MAIN