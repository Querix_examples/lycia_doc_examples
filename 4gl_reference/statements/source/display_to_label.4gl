##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

OPEN WINDOW w WITH FORM "display_to_label" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("change", "change" , "qx://application/create.svg")
  ON ACTION "change"
    DISPLAY "new text" TO lb1
    DISPLAY "qx://application/image.svg" TO lb1
    --DISPLAY "new text" TO lb2
    DISPLAY "qx://application/image.svg" TO lb2
    DISPLAY "new text" TO lb3
    --DISPLAY "qx://application/image.svg" TO lb3
    DISPLAY "new text" TO lb4
    DISPLAY "qx://application/image.svg" TO lb5
  ON ACTION  "exit"
    EXIT MENU
END MENU


END MAIN