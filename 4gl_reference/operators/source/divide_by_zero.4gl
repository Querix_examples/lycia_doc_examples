##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE a, b, c  INTEGER

WHENEVER ANY ERROR CONTINUE

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("zero", "zero" , "qx://application/zero.svg")
    CALL fgl_setactionlabel("null", "null" , "qx://application/cloud_empty.svg")
    CALL fgl_setactionlabel("sum", "sum" , "qx://application/add.svg")
  ON ACTION "zero"
    LET a = 5 / 0
    DISPLAY status
    IF a IS NULL
      THEN DISPLAY "null"
      ELSE DISPLAY "a = ", a
    END IF
  ON ACTION "null"
    LET b = NULL
    LET a = 5 / b
    DISPLAY status
    IF a IS NULL
      THEN DISPLAY "null"
      ELSE DISPLAY "a = ", a
    END IF
  ON ACTION "sum"
    LET c = a + 1
    DISPLAY "a = ", a
    DISPLAY "c = ", c
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN