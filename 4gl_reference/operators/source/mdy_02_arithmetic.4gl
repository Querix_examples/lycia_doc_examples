##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE my_date DATE
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "mdy_02_arithmetic" ATTRIBUTE(BORDER)

DISPLAY "LET my_date = MDY(06/3, 3+9, 2005)" TO lb1
LET my_date = MDY(06/3, 3+9, 2005)

LET tmp = "Your date is ", my_date
DISPLAY tmp TO lb2

CALL fgl_getkey()
END MAIN