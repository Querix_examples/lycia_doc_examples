##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE dt1 DATETIME YEAR TO SECOND
DEFINE dt2 DATETIME YEAR TO SECOND
DEFINE tmp1, tmp2 STRING

OPEN WINDOW w WITH FORM "units_02_arithmetic_md" ATTRIBUTE(BORDER)

LET dt1 = CURRENT YEAR TO SECOND
LET tmp1 = "CURRENT: ", dt1
LET tmp2 = tmp1

LET dt2 = dt1 + 5 UNITS YEAR
LET tmp1 = "Increased by 5 years: ", dt2
LET tmp2 = tmp2, "\n", tmp1 

LET dt2 = dt1 + 1 UNITS MONTH
LET tmp1 = "Increased by 1 month: ", dt2
LET tmp2 = tmp2, "\n", tmp1

LET dt2 = dt1 + 5 UNITS DAY
LET tmp1 = "Increased by 5 days: ", dt2
LET tmp2 = tmp2, "\n", tmp1

LET dt2 = dt1 + 8 UNITS HOUR
LET tmp1 = "Increased by 8 hours: ", dt2
LET tmp2 = tmp2, "\n", tmp1

LET dt2 = dt1 + 5 UNITS MINUTE
LET tmp1 = "Increased by 5 minutes: ", dt2
LET tmp2 = tmp2, "\n", tmp1

LET dt2 = dt1 + 20 UNITS SECOND
LET tmp1 = "Increased by 20 seconds: ", dt2
LET tmp2 = tmp2, "\n", tmp1
DISPLAY tmp2 TO lb1

CALL fgl_getkey()
END MAIN