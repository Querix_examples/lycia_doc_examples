##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE int_year INTERVAL YEAR TO YEAR
DEFINE int_month INTERVAL MONTH TO MONTH
DEFINE int_day INTERVAL DAY TO DAY
DEFINE int_hour INTERVAL HOUR TO HOUR
DEFINE int_min INTERVAL MINUTE TO MINUTE
DEFINE int_sec INTERVAL SECOND TO SECOND
DEFINE tmp STRING

LET int_year = 25.25 UNITS YEAR 
LET int_month = 1 UNITS MONTH 
LET int_day = 12 UNITS DAY 
LET int_hour = 8 UNITS HOUR 
LET int_min = 5 UNITS MINUTE 
LET int_sec = 20 UNITS SECOND 

LET tmp = "25 years: ", int_year, "\n",
          "1 month: ", int_month, "\n",
          "12 days: ", int_day, "\n",
          "8 hours: ", int_hour, "\n",
          "5 minutes: ", int_min, "\n",
          "20 seconds: ", int_sec
CALL fgl_winmessage("UNITS operator", tmp, "")

END MAIN