##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE day_num INTEGER
DEFINE day_name CHAR(10)
DEFINE input_var DATE
DEFINE date_var DATE
DEFINE tmp STRING

LET input_var = fgl_winprompt(5, 3, "Enter a date in this format: mm/dd/yyyy.", "", 25, 0)
IF input_var IS NOT NULL THEN
  LET date_var = DATE(input_var)
END IF

LET day_num = WEEKDAY(date_var)

CASE day_num
  WHEN 1 LET day_name = "Monday"
  WHEN 2 LET day_name = "Tuesday"
  WHEN 3 LET day_name = "Wednesday"
  WHEN 4 LET day_name = "Thursday"
  WHEN 5 LET day_name = "Friday"
  WHEN 6 LET day_name = "Saturday"
  WHEN 0 LET day_name = "Sunday"
END CASE

LET tmp = "You have entered ", trim(input_var), ".\n",
          "And it is ", trim(day_name), "."
CALL fgl_winmessage("WEEKDAY() operator", tmp, "")

END MAIN