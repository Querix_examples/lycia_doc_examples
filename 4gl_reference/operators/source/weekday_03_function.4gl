##########################################################################
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE  test_day DATE,
  print_line, i, week_day_num smallint,
  day_name char(3)
    LET test_day = TODAY
    DISPLAY "weekday() returns the weekday (0-6) of any given date" at 1,5
    DISPLAY "Date:" at 3, 5
    DISPLAY "weekday() return" at 3, 20
    DISPLAY "Day Name:" at 3, 40
  FOR i = 1 TO 21
    CALL size_theday(test_day) RETURNING day_name, week_day_num
    LET print_line = i + 3
    DISPLAY test_day at print_line, 5
    DISPLAY week_day_num at print_line, 20
    DISPLAY day_name at print_line, 40
    LET test_day = test_day + 1
  END FOR
  CALL fgl_getkey()
END MAIN

FUNCTION size_theday(test_day)
  DEFINE
    week_day_num SMALLINT,
    day_name CHAR(3),
    test_day DATE
  LET week_day_num = WEEKDAY(test_day)
  CASE week_day_num
    WHEN 1 LET day_name = "Mon"
    WHEN 2 LET day_name = "Tue"
    WHEN 3 LET day_name = "Wed"
    WHEN 4 LET day_name = "Thu"
    WHEN 5 LET day_name = "Fri"
    WHEN 6 LET day_name = "Sat"
    WHEN 0 LET day_name = "Sun"
  END CASE
 RETURN day_name,week_day_num
END FUNCTION