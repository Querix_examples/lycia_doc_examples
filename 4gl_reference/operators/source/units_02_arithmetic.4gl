##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE dt1 DATETIME YEAR TO SECOND
DEFINE dt2 DATETIME YEAR TO SECOND

LET dt1 = CURRENT YEAR TO SECOND

DISPLAY "CURRENT: ", dt1
LET dt2 = dt1 + 5 UNITS YEAR
DISPLAY "Increased by 5 years: ", dt2
LET dt2 = dt1 + 1 UNITS MONTH
DISPLAY "Increased by 1 month: ", dt2
LET dt2 = dt1 + 5 UNITS DAY
DISPLAY "Increased by 5 days: ", dt2
LET dt2 = dt1 + 8 UNITS HOUR
DISPLAY "Increased by 8 hours: ", dt2
LET dt2 = dt1 + 5 UNITS MINUTE
DISPLAY "Increased by 5 minutes: ", dt2
LET dt2 = dt1 + 20 UNITS SECOND
DISPLAY "Increased by 20 seconds: ", dt2

END MAIN