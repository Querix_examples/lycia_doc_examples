##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE int_year INTERVAL YEAR TO YEAR
DEFINE int_month INTERVAL MONTH TO MONTH
DEFINE int_day INTERVAL DAY TO DAY
DEFINE int_time INTERVAL HOUR TO SECOND

LET int_year = 25 UNITS YEAR
DISPLAY "25 years: ", int_year
LET int_month = 1 UNITS MONTH
DISPLAY "1 month: ", int_month
LET int_day = 12 UNITS DAY
DISPLAY "12 days: ", int_day
LET int_time = 8 UNITS HOUR
DISPLAY "8 hours: ", int_time
LET int_time = 5 UNITS MINUTE
DISPLAY "5 minutes: ", int_time
LET int_time = 20 UNITS SECOND
DISPLAY "20 seconds: ", int_time

END MAIN