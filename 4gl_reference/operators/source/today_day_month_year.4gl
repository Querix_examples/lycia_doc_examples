##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE my_date DATE

LET my_date = TODAY

DISPLAY "Today is ", my_date
DISPLAY "Date: ", trim(DAY(my_date))
DISPLAY "Month: ", trim(MONTH(my_date))
DISPLAY "Year: ", trim(YEAR(my_date))

END MAIN