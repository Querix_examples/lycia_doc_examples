##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE my_date DATE
DEFINE year_2d, year_4d INTEGER
DEFINE tmp STRING

LET my_date = TODAY
LET year_4d = YEAR(my_date)
LET year_2d = year_4d MOD 100 

LET tmp = "Today is ", my_date, ".\n",
          "Year's 4 digits are ", trim(year_4d), ".\n",
          "Year's 2 digits are ", trim(year_2d)
CALL fgl_winmessage("Year() operator", tmp, "")

END MAIN