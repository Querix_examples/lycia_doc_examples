##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE my_date DATE
DEFINE v_day, v_month, v_year INTEGER
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "mdy_01" ATTRIBUTE(BORDER)

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("convert" ,"convert" , "qx://application/date_range.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  ON ACTION "convert"
    LET v_month = fgl_winprompt(5, 3, "Enter an integer from 1 to 12.", "Your month", 25, 0)
    LET v_day = fgl_winprompt(5, 3, "Enter an integer from 1 to 31. Mind the month!", "Your day", 25, 0)
    LET v_year = fgl_winprompt(5, 3, "Enter a four-digit integer.", "Your year", 25, 0)
    IF v_month IS NOT NULL AND v_day IS NOT NULL AND v_year IS NOT NULL THEN
      LET my_date = MDY(v_month, v_day, v_year)
        LET tmp = "Your date is ", my_date
        DISPLAY tmp TO lb1
     ENd IF
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN