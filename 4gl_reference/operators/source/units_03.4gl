##########################################################################
# Created by Eugenia Pyzina			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE dt1 DATETIME YEAR TO SECOND
DEFINE dt2 DATETIME YEAR TO SECOND
DEFINE tmp1, tmp2, choose STRING
DEFINE input_var INTEGER

OPEN WINDOW w WITH FORM "units_03" ATTRIBUTE(BORDER)

LET dt1 = CURRENT YEAR TO SECOND

MENU
  BEFORE MENU
    CALL fgl_setactionlabel("current" ,"current" , "qx://application/calendar_today.svg")
    CALL fgl_setactionlabel("change" ,"change" , "qx://application/update.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  COMMAND "current"
    LET tmp1 = "CURRENT: ", dt1
    DISPLAY tmp1 TO lb1
  COMMAND "change"
    LET choose = UPSHIFT(fgl_winbutton("", "What do you want to change?", "sec", "sec|min|hour|day|month|year", "", 1))
    CASE choose
      WHEN "YEAR"
        LET input_var = fgl_winprompt(5, 3, "How many years do you want to add to the current date?", "", 250, 0)
        LET dt2 = dt1 + input_var UNITS YEAR
        LET tmp2 = "Increased by ", trim(input_var), " years:   ", dt2
        DISPLAY tmp2 TO lb2
      WHEN "MONTH"
        LET input_var = fgl_winprompt(5, 3, "How many months do you want to add to the current date?", "", 250, 0)
        LET dt2 = dt1 + input_var UNITS MONTH
        LET tmp2 = "Increased by ", trim(input_var), " months:   ", dt2
        DISPLAY tmp2 TO lb2
      WHEN "DAY"
        LET input_var = fgl_winprompt(5, 3, "How many days do you want to add to the current date?", "", 250, 0)
        LET dt2 = dt1 + input_var UNITS DAY
        LET tmp2 = "Increased by ", trim(input_var), " days:   ", dt2
        DISPLAY tmp2 TO lb2
      WHEN "HOUR"
        LET input_var = fgl_winprompt(5, 3, "How many hours do you want to add to the current date?", "", 250, 0)
        LET dt2 = dt1 + input_var UNITS HOUR
        LET tmp2 = "Increased by ", trim(input_var), " hours:   ", dt2
        DISPLAY tmp2 TO lb2
      WHEN "MIN"
        LET input_var = fgl_winprompt(5, 3, "How many minutes do you want to add to the current date?", "", 250, 0)
        LET dt2 = dt1 + input_var UNITS MINUTE
        LET tmp2 = "Increased by ", trim(input_var), " minutes:   ", dt2
        DISPLAY tmp2 TO lb2
      WHEN "SEC"    
        LET input_var = fgl_winprompt(5, 3, "How many seconds do you want to add to the current date?", "", 250, 0)
        LET dt2 = dt1 + input_var UNITS SECOND
        LET tmp2 = "Increased by ", trim(input_var), " seconds:   ", dt2
        DISPLAY tmp2 TO lb2
    END CASE
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN