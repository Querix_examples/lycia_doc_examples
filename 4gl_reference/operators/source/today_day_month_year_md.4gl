##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE my_date DATE
DEFINE tmp STRING

LET my_date = TODAY

LET tmp = "Today is ", my_date, "\n",
          "Date: ", trim(DAY(my_date)), "\n",
          "Month: ", trim(MONTH(my_date)), "\n",
          "Year: ", trim(YEAR(my_date))
CALL fgl_winmessage("", tmp, "")

END MAIN