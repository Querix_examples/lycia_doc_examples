##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE input_var DATE --CHAR(25)
DEFINE date_var DATE
DEFINE current_month CHAR(10)

LET current_month = CURRENT MONTH TO MONTH

LET input_var = fgl_winprompt(5, 3, "Enter a date in this format: mm/dd/yyyy.", "", 25, 0)
IF input_var IS NOT NULL THEN
  LET date_var = DATE(input_var)
END IF

IF MONTH(date_var) = 8 THEN
  DISPLAY "Your month of interest is August."
ELSE
  IF MONTH(date_var) <= 7 THEN
    DISPLAY "Your month of interest comes before August."
  ELSE
    DISPLAY "Your month of interest comes after August."
  END IF
END IF


CALL fgl_getkey()
END MAIN