##########################################################################
# Created by Eugenia Chubar			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE my_date DATE
DEFINE day_num INTEGER
DEFINE day_name CHAR(10)

LET my_date = TODAY
LET day_num = WEEKDAY(my_date)

DISPLAY "Today is ", my_date, "."
DISPLAY "WEEKDAY() returns ", trim(day_num), "."

CASE day_num
  WHEN 1 LET day_name = "Monday"
  WHEN 2 LET day_name = "Tuesday"
  WHEN 3 LET day_name = "Wednesday"
  WHEN 4 LET day_name = "Thursday"
  WHEN 5 LET day_name = "Friday"
  WHEN 6 LET day_name = "Saturday"
  WHEN 0 LET day_name = "Sunday"
END CASE

DISPLAY "So today is ", trim(day_name), "."

END MAIN