##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

CALL fgl_winmessage("Title of the message", "This is the text of the test message.", "info")
CALL fgl_winmessage("", "This is the text of the test message.", "info")
CALL fgl_winmessage("Title of the message", "", "info")
CALL fgl_winmessage("Title of the message", "This is the text of the test message.", "")
CALL fgl_winmessage("Multiple lines", "This is the 1st message line. \nThis is the 2nd message line.", "info")

END MAIN