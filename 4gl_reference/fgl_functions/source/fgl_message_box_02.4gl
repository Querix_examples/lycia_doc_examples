##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE res, btn INTEGER
DEFINE tmp STRING

LET btn = fgl_winprompt(2, 2, "To choose the set of buttons, enter a number from 0 to 4.", "", 1, 1)
LET res = fgl_message_box("","Here are your buttons:", btn) 

CASE res
	WHEN 1
		LET tmp = "Returned value: 1", "\n",
							"You have pressed OK or YES."
	WHEN 2
		LET tmp = "Returned value: 2", "\n",
							"You have pressed CANCEL."
  WHEN 3
		LET tmp = "Returned value: 3", "\n",
							"You have pressed NO or RETRY."
	EXIT CASE
END CASE

CALL fgl_winmessage("", tmp, "")

END MAIN