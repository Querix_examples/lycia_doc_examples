##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE choose STRING

LET choose = fgl_winbutton("Choose", "What do you want to test for fgl_message_box()?", "Buttons", "Buttons|Parameters", "question", 1)

IF choose = "Buttons"
THEN
	CALL fgl_message_box("Title of the message", "This is the text of the test message. \n0: OK", 0)
	CALL fgl_message_box("Title of the message", "This is the text of the test message. \n1: OK, Cancel", 1)
	CALL fgl_message_box("Title of the message", "This is the text of the test message. \n2: Retry, Cancel", 2)
	CALL fgl_message_box("Title of the message", "This is the text of the test message. \n3: Yes, No, Cancel", 3)
	CALL fgl_message_box("Title of the message", "This is the text of the test message. \n4: Yes, No", 4)
ELSE
	CALL fgl_message_box("Title of the message", "This is the 1st message line. \nThis is the 2nd message line.", 1)
	CALL fgl_message_box("", "This is the text of the test message.", 1)
	CALL fgl_message_box("Title of the message", "", 1)
	CALL fgl_message_box("Title of the message", "This is the text of the test message.")
END IF

END MAIN