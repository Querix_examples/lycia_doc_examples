MAIN
DEFINE x TEXT

OPEN WINDOW Error_log AT 5,5 WITH 3 ROWS, 25 COLUMNS
CALL startlog("errors.txt")
CALL errorlog ("Error 1 in my log")
CALL errorlog ("Error 2 in my log")
CALL errorlog ("Error 3 in my log")
CALL errorlog (err_get (-999))

LOCATE x IN FILE "errors.txt"
DISPLAY x

CALL fgl_getkey()
END MAIN