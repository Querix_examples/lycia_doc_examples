##########################################################################
# Widgets Project                                                        #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE strHash, strValue, strHashAlgorithmName STRING
    LET strValue = "1234567"
    LET strHashAlgorithmName = "crc32"
    
    LET strHash = fgl_hash(strHashAlgorithmName, strValue)
    DISPLAY strHash
  CALL fgl_getkey()
END MAIN