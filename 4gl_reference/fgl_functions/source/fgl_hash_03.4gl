##########################################################################
# Widgets Project                                                        #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE strHash, strValue, strHashAlgorithmName STRING
  DEFINE arrAllSupportedHashAlgorithmNames DYNAMIC ARRAY OF STRING
  DEFINE i INT

    LET arrAllSupportedHashAlgorithmNames = fgl_get_supported_hash_algorithms()
    DISPLAY "supported hash algorithms are: ", arrAllSupportedHashAlgorithmNames

    LET strValue = "12345"
    DISPLAY "value: ", strValue
    
    FOR i = 1 TO arrAllSupportedHashAlgorithmNames.getLength()
      LET strHashAlgorithmName = arrAllSupportedHashAlgorithmNames[ i ]
      LET strHash = fgl_hash( strHashAlgorithmName, strValue )
      DISPLAY strHashAlgorithmName, ": ", strHash
    END FOR
  CALL fgl_getkey()
END MAIN