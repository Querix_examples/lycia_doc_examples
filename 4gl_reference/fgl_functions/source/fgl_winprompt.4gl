##########################################################################
# Created by Eugenia Chubar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE value_string CHAR(25)

LET value_string = fgl_winprompt(5, 5, "Enter any string (max = 25 chars)", "Your input", 25, 0)
LET value_string = "Value entered: ", value_string
CALL fgl_winmessage("Value entered", value_string CLIPPED, "info")

END MAIN