##########################################################################
# Widgets Project                                                        #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

#The easiest way to check how arguments work is to specify them if the run configuration
#Read here - https://querix.com/go/lycia/index.htm#05_workbench/01_ls/04_how_to/06_run/run_config.htm

DEFINE i INT
MENU "args"
	BEFORE MENU
    CALL fgl_setactionlabel("program" ,"program" , "qx://application/home.svg")
    CALL fgl_setactionlabel("num_args" ,"num_args" , "qx://application/search.svg")
    CALL fgl_setactionlabel("exit" ,"exit" , "qx://application/clear.svg")
  COMMAND "program"
    DISPLAY arg_val(0) AT 5, 5 -- displays the name and the path to the application
  COMMAND "num_args"
    DISPLAY num_args()  AT 6, 5 -- displays the number of arguments passed when running the application
  COMMAND "exit"
    EXIT PROGRAM
END MENU
END MAIN