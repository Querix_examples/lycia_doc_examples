##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE choose STRING

LET choose = fgl_winbutton("Choose", "What do you want to test for fgl_winbutton()?", "Buttons", "Buttons|Parameters", "question", 1)

IF choose = "Buttons"
THEN
	CALL fgl_winbutton("fgl_winbutton()", "Two buttons.", "1", "1|2", "info", 1)
	CALL fgl_winbutton("fgl_winbutton()", "26 buttons.", "a", "a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z", "info", 1)
	CALL fgl_winbutton("fgl_winbutton()", "Button names are 5 letters long.", "abcde", "abcde|12345", "info", 1)
	CALL fgl_winbutton("fgl_winbutton()", "Button names are 26 letters long.", "", "abcdefghijklmnopqrstuvwxyz|abcdefghijklmnopqrstuvwxyz", "info", 1)
	CALL fgl_winbutton("fgl_winbutton()", "Button names consist of two words.", "two_words", "two_words|two.words|two-words|two words", "info", 1)
ELSE
	CALL fgl_winbutton("fgl_winbutton()", "This is the 1st message line. \nThis is the 2nd message line.", "A", "A|B|C|D|E", "info", 1)
	CALL fgl_winbutton("", "Choose your letter.", "A", "A|B|C|D|E", "info", 1)
	CALL fgl_winbutton("fgl_winbutton()", "", "A", "A|B|C|D|E", "info", 1)
	CALL fgl_winbutton("fgl_winbutton()", "Choose your letter.", "", "A|B|C|D|E", "info", 1)
	CALL fgl_winbutton("fgl_winbutton()", "Choose your letter.", "A", "", "info", 1)
	CALL fgl_winbutton("fgl_winbutton()", "Choose your letter.", "A", "A|B|C|D|E", "", 1)
	CALL fgl_winbutton("fgl_winbutton()", "Choose your letter.", "A", "A|B|C|D|E", "info")
END IF

END MAIN