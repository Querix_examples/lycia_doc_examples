##########################################################################
# Created by Eugenia Chubar			                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE id_var, act_var STRING
DEFINE tmp STRING

OPEN WINDOW w WITH FORM "fgl_lastaction.fgl_getlastwidgetid" ATTRIBUTE(BORDER)
MENU
	ON ACTION "accept"
	  LET act_var = fgl_lastaction()
		LET tmp = "fgl_lastaction(): ", act_var
		CALL fgl_winmessage("", tmp, "info")
	ON ACTION act_bt1
		LET act_var = fgl_lastaction()
		LET id_var = fgl_getlastwidgetid()
		LET tmp = "fgl_lastaction(): ", act_var, "\n",
              "fgl_getlastwidgetid(): ", id_var
		CALL fgl_winmessage("", tmp, "info")
	ON ACTION act_bt2
		LET act_var = fgl_lastaction()
		LET id_var = fgl_getlastwidgetid()
		LET tmp = "fgl_lastaction(): ", act_var, "\n",
              "fgl_getlastwidgetid(): ", id_var
		CALL fgl_winmessage("", tmp, "info")
	ON ACTION act_bt3
		LET act_var = fgl_lastaction()
		LET id_var = fgl_getlastwidgetid()
		LET tmp = "fgl_lastaction(): ", act_var, "\n",
              "fgl_getlastwidgetid(): ", id_var
		CALL fgl_winmessage("", tmp, "info")
	ON ACTION act_bt4
		LET act_var = fgl_lastaction()
		LET id_var = fgl_getlastwidgetid()
		LET tmp = "fgl_lastaction(): ", act_var, "\n",
              "fgl_getlastwidgetid(): ", id_var
		CALL fgl_winmessage("", tmp, "info")
	 ON ACTION exit
	 	EXIT MENU
END MENU

END MAIN