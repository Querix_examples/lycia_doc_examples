##########################################################################
# Widgets Project                                                        #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

   DEFINE
     main_menu_id,
     submenu_id,
     exit_id,
     action_id INTEGER
     

   CALL ui.Interface.setType("container")
   CALL ui.Interface.setName("window1")
   CALL ui.Application.GetCurrent().setMenuType("Tree")
   
   OPEN WINDOW w AT 1,1 WITH 24 ROWS, 80 COLUMNS
   LET main_menu_id = create_menu()
 
   LET submenu_id = menu_add_submenu(main_menu_id, "menuGroup","",555,"Group Tooltip","qx://application/folder.svg")
   CALL menu_add_option(submenu_id, "1st command", 1, 1, "This is the 1st command in this menu",  "qx://application/filter_1.svg")
   --CALL menu_add_option(submenu_id, "1st command", 1)
   
   --CALL menu_add_option(submenu_id, "-", 2)
   
   CALL menu_add_option(submenu_id, "2nd command", 2, 1,"This is the 2nd command in this menu",  "qx://application/filter_2.svg")
   CALL menu_add_option(submenu_id, "3rd command", 3, 1,"This is the 3rd command in this menu",  "qx://application/filter_3.svg")
   CALL menu_add_option(main_menu_id, "exit", 99, 1, "Exit menu","qx://application/clear.svg")

   CALL menu_publish() 
   WHILE TRUE 
      LET action_id = execute_menu()
      CASE action_id
         WHEN 1
            DISPLAY "1st command was performed" AT 5, 1
         WHEN 2 
            DISPLAY "2nd command was performed" AT 5, 1
         WHEN 3
            DISPLAY "3rd command was performed" AT 5, 1
        WHEN 99 
          EXIT WHILE 
      END CASE
   END WHILE

END MAIN