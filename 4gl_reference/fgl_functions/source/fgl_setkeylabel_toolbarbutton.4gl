##########################################################################
# Created by the Querix team		                                         #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

	DEFINE lb1 STRING

OPEN WINDOW ToolBarButton_setkeylabel WITH FORM "fgl_setkeylabel_toolbarbutton" ATTRIBUTE(BORDER)

CALL fgl_setkeylabel("button_SKL", "Button 1", "qx://application/home.svg", 1, true, "Test ToolTip" , "top")

	MENU		
		ON ACTION button_SKL
		DISPLAY "Button with \"setkeylabel\" pressed" to lb1
				
		ON ACTION act2
		DISPLAY "Button with default property pressed" to lb1
	
		ON ACTION  "exit"
		EXIT MENU			
	END MENU



END MAIN