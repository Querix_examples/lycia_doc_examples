##########################################################################
# Widgets Project                                                        #
# Property of Querix Ltd.                                                #
# Copyright (C) 2017  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

   DEFINE
     main_menu_id,
     submenu_1, submenu_2,
     exit_id,
     action_id INTEGER
     

   CALL ui.Interface.setType("container")
   CALL ui.Interface.setName("window1")
   CALL ui.Application.GetCurrent().setMenuType("Tree")
   --CALL ui.Application.GetCurrent().SetClassNames(["md-sidebar"]) - adds a sidebar
   
   OPEN WINDOW w AT 1,1 WITH 24 ROWS, 80 COLUMNS
   LET main_menu_id = create_menu()
 
   LET submenu_1 = menu_add_submenu(main_menu_id, "1st menu group", 1, 111, "", "qx://application/folder.svg")
   --LET submenu_1 = menu_add_submenu(main_menu_id, "1st menu group", "", 111)
   CALL menu_add_option(submenu_1, "1x1st command", 11, 1,"This is the 1st command in this menu",  "qx://application/filter_1.svg")
   CALL menu_add_option(submenu_1, "1x2nd command", 12, 1,"This is the 2nd command in this menu",  "qx://application/filter_2.svg")
   
   LET submenu_2 = menu_add_submenu(main_menu_id, "Second menu group", 1, 222, "This is the 2nd group in this menu", "qx://application/folder.svg")
   CALL menu_add_option(submenu_2, "2x1st command", 21, 1,"This is the 1st command in this menu",  "qx://application/filter_1.svg")
   CALL menu_add_option(submenu_2, "2x2nd command", 22, 1,"This is the 2nd command in this menu",  "qx://application/filter_2.svg")
   
   CALL menu_add_option(main_menu_id, "exit", 99, 1, "Exit menu","qx://application/clear.svg")

   CALL menu_publish() 
   WHILE TRUE 
      LET action_id = execute_menu()
      CASE action_id
         WHEN 1
            DISPLAY "1st command was performed" AT 5, 1
         WHEN 2 
            DISPLAY "2nd command was performed" AT 5, 1
         WHEN 3
            DISPLAY "3rd command was performed" AT 5, 1
        WHEN 99 
          EXIT WHILE 
      END CASE
   END WHILE

END MAIN