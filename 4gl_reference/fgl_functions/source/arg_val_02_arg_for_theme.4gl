##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE arg STRING

OPEN WINDOW w WITH FORM "arg_val_02_arg_for_theme" ATTRIBUTE(BORDER)

LET arg = arg_val(1)
CASE
  WHEN arg MATCHES "blue"
    CALL apply_theme("blue.qxtheme")
  WHEN arg MATCHES "green"
    CALL apply_theme("green.qxtheme")
END CASE

MENU
  ON ACTION "act1"
    EXIT MENU
  ON ACTION "act2"
    EXIT MENU
END MENU
  
  END MAIN