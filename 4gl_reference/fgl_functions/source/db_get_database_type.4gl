##########################################################################
# Created by Alexander Chubar			                                       #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE tmp STRING
DEFINE db_name STRING

PROMPT "enter database name" FOR db_name
DATABASE  db_name

LET tmp = "You are connected to this database:", "\n", db_get_database_type()
CALL fgl_winmessage("", tmp, "")
END MAIN