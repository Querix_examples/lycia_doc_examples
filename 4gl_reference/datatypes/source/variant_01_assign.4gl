##########################################################################
# Created by Alexander Bondar                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
   DEFINE 
      var_char     CHAR(40),
      var_datetime DATETIME YEAR TO FRACTION(5),
      var_variant  VARIANT            

   ##### VARIANT as CHAR #####
   LET var_char = "abc"
   LET var_variant = var_char 
   DISPLAY "var_variant is ", var_variant.gettypefullname(), ",   var_variant = ", var_variant

   ##### VARIANT as INT #####
   LET var_variant = 123    
   DISPLAY "var_variant is ",var_variant.gettypefullname(), ",   var_variant = ", var_variant

   ##### VARIANT as DATETIME #####
   LET var_datetime = "9999-12-31 23:59:59.99999"
   LET var_variant = var_datetime      
   DISPLAY "var_variant is ", var_variant.gettypefullname(), ",   var_variant = ", var_variant

   ##### VARIANT as STRING #####
   LET var_variant = "9999-12-31 23:59:59.99999"
   DISPLAY "var_variant is ",var_variant.gettypefullname(), ",   var_variant = ", var_variant

   ##### VARIANT as DATETIME #####
   LET var_variant = DATETIME(9999-12-31 23:59:59.99999) YEAR TO FRACTION(5) 
   DISPLAY "var_variant is ",var_variant.gettypefullname(), ",   var_variant = ",var_variant
 
CALL fgl_getkey()
END MAIN