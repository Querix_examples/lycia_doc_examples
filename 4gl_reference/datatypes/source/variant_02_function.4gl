##########################################################################
# Created by Alexander Bondar                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
   DEFINE
      var_char     CHAR(40),
      var_datetime DATETIME YEAR TO FRACTION(5),
      var_integer  INTEGER,
      var_variant  VARIANT            

   ##### VARIANT as CHAR #####
   LET var_char = "abc"
   CALL my_func(var_char) RETURNING var_char
   DISPLAY "main: var_char = ", var_char

   ##### VARIANT as INT #####
   CALL my_func(123) RETURNING var_integer 
   DISPLAY "main: var_integer = ", var_integer

   ##### VARIANT as DATETIME #####
   LET var_datetime = "9999-12-31 23:59:59.99999"
   CALL my_func(var_datetime)RETURNING var_datetime
   DISPLAY "main: var_datetime = ", var_datetime

CALL fgl_getkey()
END MAIN

FUNCTION my_func(p_variant)
DEFINE p_variant VARIANT

DISPLAY "my_func():  p_variant is ", p_variant.gettypefullname()
RETURN p_variant

END FUNCTION