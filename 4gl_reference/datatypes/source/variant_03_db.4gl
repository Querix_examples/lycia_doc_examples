##########################################################################
# Created by Alexander Bondar                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2018  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DATABASE stores
MAIN
   DEFINE
      var_char     CHAR(40),
      var_datetime DATETIME YEAR TO FRACTION(5),
      var_date     DATE,
      var_integer  INTEGER,
      var_decimal  DECIMAL(16,2),
      var_variant  VARIANT            

   WHENEVER ERROR CONTINUE
   DROP TABLE table_test
   CREATE TABLE table_test
   (
     col_char     CHAR(40),
     col_datetime DATETIME YEAR TO FRACTION(5),
     col_date     DATE,
     col_integer  INTEGER,
     col_decimal  DECIMAL(16,2)
   )

   ##### VARIANT as CHAR #####
   LET var_char = "abc"
   LET var_variant = var_char 
   INSERT INTO table_test(col_char) VALUES(var_variant)
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ", SQLCA.SQLCODE END IF
   SELECT col_char INTO var_variant FROM table_test WHERE col_char = var_variant
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ", SQLCA.SQLCODE END IF
   DISPLAY "var_variant is ",var_variant.gettypefullname(),",  var_variant = ",var_variant

   ##### VARIANT as DATETIME #####
   LET var_datetime = "9999-12-31 23:59:59.99999"
   LET var_variant = var_datetime 
   INSERT INTO table_test(col_datetime) VALUES(var_variant)
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
   SELECT col_datetime INTO var_variant FROM table_test WHERE col_datetime = var_variant
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
   DISPLAY "var_variant is ",var_variant.gettypefullname(),",  var_variant = ",var_variant

   ##### VARIANT as DATE #####
   LET var_date = "12/31/9999"
   LET var_variant = var_date 
   INSERT INTO table_test(col_date) VALUES(var_variant)
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
   SELECT col_date INTO var_variant FROM table_test WHERE col_date = var_variant
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
   DISPLAY "var_variant is ",var_variant.gettypefullname(),",  var_variant = ",var_variant

   ##### VARIANT as INT #####
   LET var_integer = 123
   LET var_variant = var_integer 
   INSERT INTO table_test(col_integer) VALUES(var_variant)
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
   SELECT col_integer INTO var_variant FROM table_test WHERE col_integer = var_variant
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
   DISPLAY "var_variant is ",var_variant.gettypefullname(),",  var_variant = ",var_variant

   ##### VARIANT as DECIMAL #####
   LET var_decimal = 123.45
   LET var_variant = var_decimal 
   INSERT INTO table_test(col_decimal) VALUES(var_variant)
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
   SELECT col_decimal INTO var_variant FROM table_test WHERE col_decimal = var_variant
   IF SQLCA.SQLCODE <> 0 THEN DISPLAY "SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
   DISPLAY "var_variant is ",var_variant.gettypefullname(),",  var_variant = ",var_variant

END MAIN