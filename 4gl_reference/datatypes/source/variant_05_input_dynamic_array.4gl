MAIN
  DEFINE tableRec DYNAMIC ARRAY OF DYNAMIC ARRAY OF VARIANT
  DEFINE numCol, numRow INTEGER
  DEFINE f1 BOOL,
		f2 STRING,
		f3 DATE,
		f4 SMALLINT,
		f5 INT,
		f6 INT,
		f7 INT,
		f8 INT,
		f9 CHAR(20),
		f10 DATETIME HOUR TO SECOND


	LET f1 = 1
	LET f2 = "functionfield"
	LET f3 ="12.12.2012"
	LET f4 = 20
	LET f5 = 5
	LET f6 = 50
	LET f7 = 50
	LET f8 = 50
	LET f9 = "textfield"
	LET f10 = "15:22:33"

	OPEN WINDOW w WITH FORM "variant_05_input_dynamic_array"  ATTRIBUTE(BORDER)

	FOR numRow=1 TO 5
		FOR numCol=1 TO 10
			CASE numCol
				WHEN 1	LET tableRec[numRow][numCol]=f1
			    WHEN 2	LET tableRec[numRow][numCol]=f2||" "||numRow
			    WHEN 3  LET tableRec[numRow][numCol]=f3
			    WHEN 4  LET tableRec[numRow][numCol]=f4
			    WHEN 5  LET tableRec[numRow][numCol]=f5
			    WHEN 6  LET tableRec[numRow][numCol]=f6
			    WHEN 7  LET tableRec[numRow][numCol]=f7
			    WHEN 8  LET tableRec[numRow][numCol]=f8
			    WHEN 9  LET tableRec[numRow][numCol]=f9
			    WHEN 10  LET tableRec[numRow][numCol]=f10
			END CASE
		END FOR
	END FOR
  
  INPUT ARRAY tableRec FROM tarr.* ATTRIBUTE (WITHOUT DEFAULTS)
  DISPLAY "INPUT ARRAY:",tableRec
END MAIN
