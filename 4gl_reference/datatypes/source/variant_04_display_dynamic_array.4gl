MAIN
  DEFINE DisplArr DYNAMIC ARRAY OF VARIANT
  DEFINE str STRING
  DEFINE tm DATETIME HOUR TO SECOND
  DEFINE dt DATE
  
  LET str = "string value"
  LET tm  = "15:22:35"  
  LET dt  = "01.05.2018"
  OPEN WINDOW w WITH FORM "variant_04_display_dynamic_array"  ATTRIBUTE(BORDER)

  LET DisplArr[1]= str
  LET DisplArr[2]= tm
  LET DisplArr[3]= dt

  DISPLAY DisplArr TO rec.* 
  CALL fgl_getkey()
  DISPLAY "ARRAY:",DisplArr
END MAIN
