import java java.util.logging.Level
import java org.eclipse.birt.core.framework.Platform
import java org.eclipse.birt.report.engine.api.EngineConfig
import java org.eclipse.birt.report.engine.api.HTMLRenderOption
import java org.eclipse.birt.report.engine.api.IReportEngine
import java org.eclipse.birt.report.engine.api.IReportEngineFactory
import java org.eclipse.birt.report.engine.api.IReportRunnable
import java org.eclipse.birt.report.engine.api.IRunAndRenderTask

main
	define config EngineConfig
	define factory IReportEngineFactory
	define engine IReportEngine
	define design IReportRunnable  
	define task IRunAndRenderTask
	define html_opts HTMLRenderOption
	define birt_home_dir string
    	   
    let birt_home_dir = fgl_getenv("LYCIA_DIR") || "\ReportEngine"
	let config = EngineConfig.create()
	call config.setBIRTHome(birt_home_dir)	
	call Platform.startup(config) 
	        
    let factory = Platform.createFactoryObject("org.eclipse.birt.report.engine.ReportEngineFactory")
	let engine = factory.createReportEngine(config)
	
	--open BIRT report form
	let design = engine.openReportDesign("birt_no_param.rptdesign")
	let task = engine.createRunAndRenderTask(design) 
	-- Set query parameter to  adress_menu defined in form report(see birt form in birt perspective)
    -- Remove these two CALLs if erport is simple, without parameters 
    --CALL task.setParameterValue("address_menu","Red%")	         
    --call task.validateParameters()
    
    --creates HTML as output file
	let html_opts = HTMLRenderOption.create() 
	call html_opts.setOutputFileName("birt_no_param.html")    
	
	call task.setRenderOption(html_opts)
    display "HTML render task is running..."
	call task.run()

	call task.close()
	call engine.destroy()	        
	call Platform.shutdown()
    display "Report has been saved into file:"
    display "C:\\ProgramData\\Querix\\Lycia\\progs\\birt_with_param.html"
end main
