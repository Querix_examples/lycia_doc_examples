##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE json_arr util.JSONArray
DEFINE da DYNAMIC ARRAY OF INTEGER

LET json_arr = util.JSONArray.create()
LET da[1] = 1
LET da[2] = 12
LET da[3] = 123

CALL json_arr.put(1, da)
DISPLAY json_arr.toString()  

CALL fgl_getkey()
END MAIN