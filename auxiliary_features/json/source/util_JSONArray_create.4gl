##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_arr util.JSONArray
    IF json_arr IS NOT NULL THEN DISPLAY "Failed to DEFINE the array" END IF
      LET json_arr = util.JSONArray.create()
    IF json_arr IS NULL THEN DISPLAY "Failed to CREATE the array" ELSE 
      DISPLAY "PASSED"
    END IF
  CALL fgl_getkey()
END MAIN