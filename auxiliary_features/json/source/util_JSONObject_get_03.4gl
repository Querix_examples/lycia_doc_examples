##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE json_obj util.JSONObject
DEFINE arr DYNAMIC ARRAY OF INTEGER
DEFINE json_arr util.JSONArray

LET json_obj = util.JSONObject.create()
LET arr[1] = 234
LET arr[2] = 2837
CALL json_obj.put("cust_arr", arr)
DISPLAY json_obj.toString()
LET json_arr = json_obj.get("cust_arr")
DISPLAY json_arr.toString()

CALL fgl_getkey()
END MAIN