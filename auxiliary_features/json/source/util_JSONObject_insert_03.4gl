##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE json_obj util.JSONObject
DEFINE cust_rec RECORD
       cust_id INTEGER,
       cust_name STRING
       END RECORD

LET json_obj = util.JSONObject.create()
LET cust_rec.cust_id = 15
LET cust_rec.cust_name = "Barton, Timothy"

CALL json_obj.insert("record", cust_rec)
DISPLAY json_obj.toString()  

CALL fgl_getkey()
END MAIN