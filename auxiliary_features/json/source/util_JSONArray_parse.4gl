##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
 DEFINE js STRING
 DEFINE json_obj util.JSONObject

   LET js = '{ "cust_num":273, "cust_name":"McCarlson",
             "orderids":[234,3456,24656,34561],
             "js_obj" : { "js_cust_num":1111, "js_cust_name":"js_McCarlson",
             "js_orderids":[123,2345,34567,45678] } }'
   LET json_obj = util.JSONObject.parse(js)

   DISPLAY json_obj.get("cust_num")
   DISPLAY json_obj.get("cust_name")
   DISPLAY json_obj.get("js_obj").toString()

 CALL fgl_getkey()
END MAIN