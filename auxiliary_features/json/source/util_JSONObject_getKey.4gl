##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_obj util.JSONObject
    LET json_obj = util.JSONObject.parse('{"id":12,"name":"Frank","surname":"Scott"}')

    DISPLAY json_obj.getKey(1) 
    DISPLAY json_obj.getKey(2) 
    DISPLAY json_obj.getKey(3) 
   
  CALL fgl_getkey()
END MAIN