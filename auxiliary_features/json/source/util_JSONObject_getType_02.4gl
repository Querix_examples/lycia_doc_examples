##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE json_obj util.JSONObject
DEFINE i INTEGER

LET json_obj = util.JSONObject.parse('{"id":12,"name":"Scott, Frank", "address":"5 Brando Str."}')

FOR i = 1 TO json_obj.getLength()
  DISPLAY i, ": ", json_obj.name(i), " = ", json_obj.get(json_obj.name(i)), ": ", json_obj.getType("name")
END FOR

CALL fgl_getkey()
END MAIN