##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE json_obj util.JSONObject
DEFINE js STRING

LET json_obj = util.JSONObject.create()

CALL json_obj.put("id", 8723)
CALL json_obj.put("name", "McMaclum")
CALL json_obj.put("address", "5 Brando Street")
CALL json_obj.put("position", "staff")
LET js =  json_obj.toString()
DISPLAY util.JSON.format(js)

CALL json_obj.remove("address")
LET js =  json_obj.toString()
DISPLAY util.JSON.format(js)

CALL fgl_getkey()
END MAIN