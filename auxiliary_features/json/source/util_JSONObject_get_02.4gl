##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE json_obj, json_res_obj util.JSONObject
DEFINE cust_rec RECORD
         cust_num INTEGER,
         cust_name VARCHAR(30),
         order_ids DYNAMIC ARRAY OF INTEGER
       END RECORD
DEFINE man_rec RECORD
         man_num INTEGER,
         man_name VARCHAR(30)
       END RECORD

LET json_obj = util.JSONObject.create()
LET cust_rec.cust_num = 345
LET cust_rec.cust_name = "McMaclum"
LET cust_rec.order_ids[1] = 4732
LET cust_rec.order_ids[2] = 9834
LET cust_rec.order_ids[3] = 2194
CALL json_obj.put("Object_cust", cust_rec)

LET man_rec.man_num = 123
LET man_rec.man_name = "Black"
CALL json_obj.put("Object_man", man_rec)

LET json_res_obj = json_obj.get("Object_man")
DISPLAY json_res_obj.toString()

CALL fgl_getkey()
END MAIN