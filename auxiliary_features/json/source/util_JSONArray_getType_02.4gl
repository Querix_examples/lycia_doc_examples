##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_arr util.JSONArray
  DEFINE i INTEGER
    LET json_arr = util.JSONArray.parse('[123,"abc",null]')
    
    FOR i = 1 TO json_arr.getLength()
      DISPLAY i, ": ", json_arr.getType(i)
    END FOR
  CALL fgl_getkey()	
END MAIN