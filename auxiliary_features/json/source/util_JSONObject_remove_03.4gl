##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE json_obj util.JSONObject
DEFINE cust_rec RECORD
       cust_num INTEGER,
       cust_name VARCHAR(30),
       cust_address VARCHAR(30),
       order_ids DYNAMIC ARRAY OF INTEGER
       END RECORD
DEFINE js STRING

LET cust_rec.cust_num = 345
LET cust_rec.cust_name = "McMaclum"
LET cust_rec.cust_address = "5 Brando Street"
LET cust_rec.order_ids[1] = 4732
LET cust_rec.order_ids[2] = 9834

LET json_obj = util.JSONObject.fromFGL(cust_rec)
LET js =  json_obj.toString()
DISPLAY util.JSON.format(js)

CALL json_obj.remove("cust_address")
LET js =  json_obj.toString()
DISPLAY util.JSON.format(js)

CALL fgl_getkey()
END MAIN