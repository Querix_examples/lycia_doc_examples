##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE obj util.JSONObject
DEFINE errorMessage STRING

LET obj = util.JSONObject.CREATE()
CALL obj.put("id", 8723)
CALL obj.put("name", "Brando")
CALL obj.put("position", NULL)

LET errorMessage = ""

IF obj.has("id") != 1 THEN
  LET errorMessage = errorMessage.APPEND("Step 1. Property  should be \n")
END IF

IF obj.has("name") != TRUE THEN
  LET errorMessage = errorMessage.APPEND("Step 2. Property  should be \n")
END IF

IF obj.has("Brando") != 0 THEN
  LET errorMessage = errorMessage.APPEND("Step 3. Property  should not be \n")
END IF

IF obj.has(8723) != FALSE THEN
  LET errorMessage = errorMessage.APPEND("Step 4. Property  should not be \n")
END IF
  
IF obj.has(NULL) != 0 THEN
  LET errorMessage = errorMessage.APPEND("Step 5. Property  should not be \n")
END IF

IF obj.has("undefine") != FALSE THEN
  LET errorMessage = errorMessage.APPEND("Step 6. Property  should not be \n")
END IF

IF errorMessage IS NULL THEN
		DISPLAY "PASSED"
	ELSE 
		DISPLAY errorMessage
END IF
	
END MAIN