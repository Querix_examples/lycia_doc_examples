##########################################################################
# Created by Eugenia Pyzina                                              #
# Property of Querix Ltd.                                                #
# Copyright (C) 2019  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE json_obj util.JSONObject

LET json_obj = util.JSONObject.create()

CALL json_obj.put("id", 8723)
CALL json_obj.put("name", "Brando")
CALL json_obj.put("position", "Manager")

IF json_obj.has("id") = TRUE THEN 
  	DISPLAY json_obj.get("id")
  ELSE 
    DISPLAY "No ID property"
END IF

IF json_obj.has("name") = TRUE THEN 
    DISPLAY json_obj.get("name")
  ELSE 
    DISPLAY "No NAME property"
END IF 

IF json_obj.has("position") = TRUE THEN 
    DISPLAY json_obj.get("position")
  ELSE 
    DISPLAY "No POSITION property"
END IF

CALL fgl_getkey()
END MAIN